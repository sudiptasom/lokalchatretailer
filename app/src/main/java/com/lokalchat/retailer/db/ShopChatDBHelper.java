package com.lokalchat.retailer.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.lokalchat.retailer.models.ProductModel;
import com.lokalchat.retailer.models.RetailerModel;
import com.lokalchat.retailer.utils.Constants;

/**
 * Created by Sudipta on 11/4/2015.
 */
public class ShopChatDBHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "ShopChatDB";
    public static final String TABLE_NAME = "ChatTable";
    public static final String COL_ID = "id";
    public static final String COL_CHAT_ID = "chat_id";
    public static final String COL_QUESTION_ID = "question_id";
    public static final String COL_PRODUCT_ID = "prod_id";
    public static final String COL_PRODUCT_NAME = "prod_name";
    public static final String COL_LAST_MODIFIED_DATE = "last_modify_date";
    public static final String COL_CON_CHAT_CONTENT = "consumer_chat_content";
    public static final String COL_RETAILER_ID = "retailer_id";
    public static final String COL_SHOP_NAME = "shop_name";
    public static final String COL_RET_CHAT_CONTENT = "retailer_chat_content";
    public static final String COL_HAS_READ = "has_read";
    public static final String COL_HAS_REPLIED = "has_replied";


    public ShopChatDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_CHAT_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME +
                " ( chat_id TEXT, " +
                "question_id TEXT, " +
                "prod_id TEXT, " +
                "prod_name TEXT, " +
                "last_modify_date TEXT, " +
                "consumer_chat_content TEXT, " +
                "retailer_id TEXT, " +
                "shop_name TEXT, " +
                "retailer_chat_content TEXT, " +
                "has_read INTEGER, " +
                "has_replied INTEGER )";

        db.execSQL(CREATE_CHAT_TABLE);
        Log.d(Constants.TAG, "Table Created");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void insertData(ProductModel productModel) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_CHAT_ID, productModel.getConsumerChatId());
        contentValues.put(COL_QUESTION_ID, productModel.getQuestionId());
        contentValues.put(COL_PRODUCT_ID, productModel.getProductId());
        contentValues.put(COL_PRODUCT_NAME, productModel.getProductName());
        contentValues.put(COL_LAST_MODIFIED_DATE, productModel.getLastModifiedDate());
        contentValues.put(COL_CON_CHAT_CONTENT, productModel.getConsumerChatContent());
        contentValues.put(COL_RETAILER_ID, ((RetailerModel) productModel.getRetailerModels().get(0)).getRetailerId());
        contentValues.put(COL_SHOP_NAME, ((RetailerModel) productModel.getRetailerModels().get(0)).getRetailerName());
        contentValues.put(COL_RET_CHAT_CONTENT, ((RetailerModel) productModel.getRetailerModels().get(0)).getRetailerChatContent());
        contentValues.put(COL_HAS_READ, 0);
        contentValues.put(COL_HAS_REPLIED, 0);

        getWritableDatabase().insert(TABLE_NAME, null, contentValues);

    }

    public void updateHasRead(String chatId, int value) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_HAS_READ, value);

        getWritableDatabase().update(TABLE_NAME, contentValues, "chat_id = ?", new String[]{chatId});
    }

    public void updateHasReplied(String chatId, int value) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_HAS_REPLIED, value);

        getWritableDatabase().update(TABLE_NAME, contentValues, "chat_id = ?", new String[]{chatId});
    }

    public int getCount() {
        int count = 0;

        String query = "SELECT  * FROM " + TABLE_NAME;

        Cursor cursor = getWritableDatabase().rawQuery(query, null);
        count = cursor.getCount();
        cursor.close();

        return count;
    }

    public Cursor getAllRecords() {
        String query = "SELECT  * FROM " + TABLE_NAME;

        Cursor cursor = getWritableDatabase().rawQuery(query, null);
        cursor.close();
        return cursor;
    }

    public Cursor getAllRecordsOrderByTimeStamp(String limit) {
        String query = "SELECT  * FROM " + TABLE_NAME + " ORDER BY " + COL_LAST_MODIFIED_DATE + " DESC " + "LIMIT " + limit;

        Cursor cursor = getWritableDatabase().rawQuery(query, null);
        return cursor;
    }

    public Cursor getByColumnName(String columnName, String value) {
        Cursor cursor = null;
        String query = "SELECT " + columnName + " FROM " + TABLE_NAME + " WHERE " + columnName + "=" + "'" + value + "'";
        cursor = getWritableDatabase().rawQuery(query, null);
        return cursor;

    }

    public void deleteAllData() {
        getWritableDatabase().execSQL("DELETE FROM " + TABLE_NAME + ";");
    }

    public void deleteByChatId(String chatId) {
        int flag = getWritableDatabase().delete(TABLE_NAME, COL_CHAT_ID + "=" + "'" + chatId + "'", null);
    }

    public int getRecordId(Cursor cursor) {
        return cursor.getInt(cursor.getColumnIndex(COL_ID));
    }

    public String getChatId(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(COL_CHAT_ID));
    }

    public String getQuestionId(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(COL_QUESTION_ID));
    }

    public String getProductId(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(COL_PRODUCT_ID));
    }

    public String getProductName(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(COL_PRODUCT_NAME));
    }

    public String getLastModifiedDate(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(COL_LAST_MODIFIED_DATE));
    }

    public String getConsumerChatContent(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(COL_CON_CHAT_CONTENT));
    }

    public String getRetailerId(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(COL_RETAILER_ID));
    }

    public String getShopName(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(COL_SHOP_NAME));
    }

    public String getRetChatContent(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(COL_RET_CHAT_CONTENT));
    }

    public boolean getHasRead(Cursor cursor) {
        boolean flag = false;

        if (cursor.getInt(cursor.getColumnIndex(COL_HAS_READ)) > 0) {
            flag = true;
        }
        return flag;
    }

    public int getHasReplied(Cursor cursor) {
        return cursor.getInt(cursor.getColumnIndex(COL_HAS_REPLIED));
    }

}
