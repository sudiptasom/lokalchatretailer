package com.lokalchat.retailer.db;

import android.content.Context;
import android.database.Cursor;

import com.lokalchat.retailer.models.ProductModel;
import com.lokalchat.retailer.models.RetailerModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sudipta on 11/8/2015.
 */
public class DBAdapter {

    public static void insertRecords(Context context, ProductModel productModel) {
        ShopChatDBHelper shopChatDBHelper = new ShopChatDBHelper(context);
        shopChatDBHelper.insertData(productModel);
        shopChatDBHelper.close();
    }

    public static int getRecordsCount(Context context) {
        int records = 0;
        ShopChatDBHelper shopChatDBHelper = new ShopChatDBHelper(context);
        records = shopChatDBHelper.getCount();
        shopChatDBHelper.close();
        return records;
    }

    public static void updateChatRead(Context context, String chatId, boolean isRead) {
        ShopChatDBHelper shopChatDBHelper = new ShopChatDBHelper(context);
        int value;
        if (isRead) {
            value = 1;
        } else {
            value = 0;
        }
        shopChatDBHelper.updateHasRead(chatId, value);
        shopChatDBHelper.close();
    }

    public static boolean hasChatRead(Context context, String chatId){
        boolean hasRead = false;
        ShopChatDBHelper shopChatDBHelper = new ShopChatDBHelper(context);
        Cursor cursor = shopChatDBHelper.getByColumnName(ShopChatDBHelper.COL_CHAT_ID, chatId);
        hasRead = shopChatDBHelper.getHasRead(cursor);
        return  hasRead;
    }

    public static void deleteRecords(Context context) {

    }

    public static boolean isChatExists(Context context, String value) {
        boolean flag = false;
        ShopChatDBHelper shopChatDBHelper = new ShopChatDBHelper(context);
        Cursor cursor = shopChatDBHelper.getByColumnName(ShopChatDBHelper.COL_CHAT_ID, value);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                flag = true;
            } else {
                flag = false;
            }
        }

        cursor.close();
        shopChatDBHelper.close();

        return flag;
    }

    public static List<ProductModel> getChatRecords(Context context, int limit) {
        List<ProductModel> chatList = new ArrayList<ProductModel>();
        ShopChatDBHelper shopChatDBHelper = new ShopChatDBHelper(context);
        Cursor cursor = shopChatDBHelper.getAllRecordsOrderByTimeStamp(String.valueOf(limit));
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    ProductModel productModel = new ProductModel();
                    productModel.setConsumerChatId(shopChatDBHelper.getChatId(cursor));
                    productModel.setQuestionId(shopChatDBHelper.getQuestionId(cursor));
                    productModel.setProductId(shopChatDBHelper.getProductId(cursor));
                    productModel.setProductName(shopChatDBHelper.getProductName(cursor));
                    productModel.setLastModifiedDate(shopChatDBHelper.getLastModifiedDate(cursor));
                    productModel.setConsumerChatContent(shopChatDBHelper.getConsumerChatContent(cursor));
                    productModel.setIsRead(shopChatDBHelper.getHasRead(cursor));

                    List<RetailerModel> retailerModelList = new ArrayList<RetailerModel>();
                    RetailerModel retailerModel = new RetailerModel();
                    retailerModel.setRetailerChatContent(shopChatDBHelper.getRetChatContent(cursor));
                    retailerModelList.add(retailerModel);
                    productModel.setRetailerModels(retailerModelList);


                    chatList.add(productModel);
                } while (cursor.moveToNext());
            }
        }

        cursor.close();
        shopChatDBHelper.close();
        return chatList;

    }

    public static void deleteRepliedChatFromDB(Context context, String chatId) {
        ShopChatDBHelper shopChatDBHelper = new ShopChatDBHelper(context);
        shopChatDBHelper.deleteByChatId(chatId);
        shopChatDBHelper.close();
    }
}
