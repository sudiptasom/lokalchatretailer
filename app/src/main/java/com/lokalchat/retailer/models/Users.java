package com.lokalchat.retailer.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sudipta on 9/7/2015.
 */
public class Users {
    @SerializedName("Users")
    private List<User> userList;
}
