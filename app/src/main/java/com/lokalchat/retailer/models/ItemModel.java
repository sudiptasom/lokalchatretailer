package com.lokalchat.retailer.models;

/**
 * Created by Sudipta on 9/12/2015.
 */
public class ItemModel {

    private String itemName;

    public ItemModel(String itemName) {
        this.itemName = itemName;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
}
