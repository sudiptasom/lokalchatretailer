package com.lokalchat.retailer.models;

/**
 * Created by Sudipta on 9/29/2015.
 */
public class ChatDisplayModel {

    private String productId;
    private String productName;
    private String retailerConsolidatedChatContent;
    private String timeStamp;
    private boolean isRead = false;
    private String consumerChatContent;
    private String chatId;
    private String questionId;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getRetailerConsolidatedChatContent() {
        return retailerConsolidatedChatContent;
    }

    public void setRetailerConsolidatedChatContent(String retailerConsolidatedChatContent) {
        this.retailerConsolidatedChatContent = retailerConsolidatedChatContent;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setIsRead(boolean isRead) {
        this.isRead = isRead;
    }

    public String getConsumerChatContent() {
        return consumerChatContent;
    }

    public void setConsumerChatContent(String consumerChatContent) {
        this.consumerChatContent = consumerChatContent;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }
}
