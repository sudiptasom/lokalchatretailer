package com.lokalchat.retailer.models;

/**
 * Created by Sudipta on 9/13/2015.
 */
public class HomeItemModel {
    private String itemName;
    private int itemPicture;

    public HomeItemModel(String itemName, int itemPicture) {
        this.itemName = itemName;
        this.itemPicture = itemPicture;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getItemPicture() {
        return itemPicture;
    }

    public void setItemPicture(int itemPicture) {
        this.itemPicture = itemPicture;
    }
}
