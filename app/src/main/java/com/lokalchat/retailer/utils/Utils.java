package com.lokalchat.retailer.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.lokalchat.retailer.R;
import com.lokalchat.retailer.ShopChatApplication;
import com.lokalchat.retailer.activities.SplashActivity;
import com.lokalchat.retailer.views.CustomProgress;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Sudipta on 8/30/2015.
 */
public class Utils {

    /**
     * @param context
     * @return
     */
    public static String getDevicePhoneNumber(Context context) {
        String phoneNumber = null;
        TelephonyManager tMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        phoneNumber = tMgr.getLine1Number();
        return phoneNumber;

    }

    /**
     * @param context
     * @param subject
     * @param body
     */
    public static void callShareIntent(Context context, String subject, String body) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, body);
        context.startActivity(Intent.createChooser(intent, "Share with"));
    }

    /**
     * @param context
     */
    public static void showExitConfirmationDialog(final Activity context) {
        new AlertDialog.Builder(context)
                .setMessage(R.string.exit_msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        context.finish();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .show();
    }

    /**
     * @param context
     */
    public static void showLogoutConfirmationDialog(final Activity context) {
        new AlertDialog.Builder(context)
                .setMessage(R.string.logout_msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        clearLoginPersistenceData(context);
                        context.finish();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .show();
    }

    private static void clearLoginPersistenceData(Activity context) {
        Utils.clearPreference(context, Constants.LOGIN_USER_NAME_PREFERENCE);
        Utils.clearPreference(context, Constants.LOGIN_PASSWORD_PREFERENCE);
    }

    /**
     * @param context
     */


    public static void showGenericDialog(Context context, int msg, boolean isActivityDestroyed) {
        if (!isActivityDestroyed) {
            new AlertDialog.Builder(context)
                    .setMessage(msg)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .show();
        }

    }

    public static void showGenericDialog(final Context context, String msg, boolean isActivityDestroyed) {
        if (!isActivityDestroyed) {
            new AlertDialog.Builder(context)
                    .setMessage(msg)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if (context instanceof SplashActivity) {
                                ((SplashActivity) context).finish();
                            }
                        }
                    })
                    .show();
        }
    }

    public static void showNetworkDisableDialog(final Context context, String msg, boolean isActivityDestroyed) {
        if (!isActivityDestroyed) {
            new AlertDialog.Builder(context)
                    .setMessage(msg)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            context.startActivity(new Intent(android.provider.Settings.ACTION_SETTINGS));
                        }
                    })
                    .show();
        }
    }

    /**
     * @param context
     * @param key
     * @param value
     */
    public static void setPersistenceData(Context context, String key, String value) {
        SharedPreferences.Editor editor = getEditor(context);
        editor.putString(key, value);
        editor.commit();
    }

    /**
     * @param context
     * @param key
     * @return
     */
    public static String getPersistenceData(Context context, String key) {
        String value = null;
        SharedPreferences sharedPreferences = getShredPreference(context);
        value = sharedPreferences.getString(key, "");
        return value;
    }

    /**
     * @param context
     * @param key
     * @param value
     */
    public static void setPersistenceBoolean(Context context, String key, boolean value) {
        SharedPreferences.Editor editor = getEditor(context);
        editor.putBoolean(key, value);
        editor.commit();
    }

    /**
     * @param context
     * @param key
     * @return
     */
    public static boolean getPersistenceBoolean(Context context, String key) {
        boolean value;
        SharedPreferences sharedPreferences = getShredPreference(context);
        value = sharedPreferences.getBoolean(key, false);
        return value;
    }

    /**
     * @param context
     * @return
     */
    public static SharedPreferences.Editor getEditor(Context context) {
        SharedPreferences sharedPreferences = getShredPreference(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        return editor;
    }

    /**
     * @param context
     * @return
     */
    public static SharedPreferences getShredPreference(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.SHOP_CHAT_PREFERENCE, Context.MODE_PRIVATE);
        return sharedPreferences;
    }

    public static void clearPreference(Context context, String key) {
        SharedPreferences settings = context.getSharedPreferences(Constants.SHOP_CHAT_PREFERENCE, Context.MODE_PRIVATE);
        if (settings.contains(key)) {
            SharedPreferences.Editor editor = settings.edit();
            editor.remove(key);
            editor.commit();
        }
    }

    public static void hideKeyBoard(Activity context, View view) {
        view = context.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void hideKeyBoard(Activity context) {
        if (context.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(context.getCurrentFocus().getWindowToken(), 0);
        }
    }

    /**
     * @param context
     * @return
     */
    public static boolean isConnectionPossible(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return (netInfo != null && netInfo.isConnected());

    }

    public static ShopChatApplication getShopChatApplication(Activity context) {
        return (ShopChatApplication) context.getApplication();
    }

    public static CustomProgress getProgressDialog(Context context) {
        // TODO Localization
        CustomProgress progressDialog = new CustomProgress(context, "Please wait");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    public static String getFirstCharacter(String string) {
        String character = "";
        if (!TextUtils.isEmpty(string)) {
            character = string.substring(0, 1);
        }

        return character.toUpperCase();
    }

    public static String getChatTimeStamp() {
        Calendar calendar = Calendar.getInstance();
        String month = calendar.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.getDefault());
        String date = String.valueOf(calendar.get(Calendar.DATE));

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        String time = sdf.format(calendar.getTime());

        return new StringBuilder().append(date).append(" ").append(month).append(" ").append(time).toString();
    }

    public static String getDigitFromSms(String msg) {
        return msg.replaceAll("[^0-9]", "").trim();
    }

    /**
     * Returns a formatted string representing the date of the passed calendar.
     * ie: December 12, 2019
     * <p/>
     * If the cal represents today's date, then it shows "Today"
     */
    public static String getDateDisplayString(long timestamp) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp);
        TimeZone tz = TimeZone.getDefault();
        cal.add(Calendar.MILLISECOND, tz.getOffset(cal.getTimeInMillis()));
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMAT_DATE);
        SimpleDateFormat sdfHrs = new SimpleDateFormat(Constants.FORMAT_HRS);
        Date date = (Date) cal.getTime();

        String dateString;
        if (isToday(cal)) {
            // TODO Localization
            dateString = "Today" + ", " + sdfHrs.format(date);
        } else {
            dateString = sdf.format(date);
        }

        return dateString;
    }

    /**
     * @return true if cal represents a time on today's date.
     */
    public static boolean isToday(Calendar cal) {
        TimeZone timeZone = cal.getTimeZone();
        Calendar today = Calendar.getInstance(timeZone);
        return isSameDay(cal, today);
    }

    /**
     * @return true if cal1 and cal2 represent a time in the same day.
     */
    public static boolean isSameDay(Calendar cal1, Calendar cal2) {
        return cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) && cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR);
    }

    public static Calendar getCalendarFromEpoch(int timeStamp) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timeStamp * 1000L);
        return cal;
    }

    public static String getApplicationPackageName(Context context) {
        return context.getApplicationContext().getPackageName();
    }

    public static String getSharableContent(Context context) {
        return Constants.PLAY_STORE_URL;
    }
}
