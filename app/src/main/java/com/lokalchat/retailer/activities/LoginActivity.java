package com.lokalchat.retailer.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.lokalchat.retailer.R;
import com.lokalchat.retailer.listener.AuthenticationListener;
import com.lokalchat.retailer.listener.CategoryListener;
import com.lokalchat.retailer.listener.PushTokenRegListener;
import com.lokalchat.retailer.models.AuthenticationModel;
import com.lokalchat.retailer.models.CategoryModel;
import com.lokalchat.retailer.models.ErrorModel;
import com.lokalchat.retailer.pushnotification.PushNotificationHelper;
import com.lokalchat.retailer.pushnotification.PushNotificationIntentService;
import com.lokalchat.retailer.pushnotification.PushResultReceiver;
import com.lokalchat.retailer.task.AuthenticationTask;
import com.lokalchat.retailer.task.CategoryTask;
import com.lokalchat.retailer.task.PushTokenRegTask;
import com.lokalchat.retailer.utils.Constants;
import com.lokalchat.retailer.utils.Utils;
import com.lokalchat.retailer.views.CustomProgress;

import java.util.List;


/**
 * Created by Sudipta on 9/5/2015.
 */
public class LoginActivity extends BaseActivity implements PushResultReceiver.PushIntentReceiver {

    public static final String PHONE_NUMBER = "phone_number";

    private CustomProgress progressDialog;
    private EditText editUserName;
    private EditText editPassword;
    private CheckBox checkBoxRememberMe;
    private boolean hasActivityDestroyed;
    private PushResultReceiver pushResultReceiver;
    private boolean isFromInbox;


    public static Intent getIntent(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        return intent;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setUpSupportActionBar(false, false, getString(R.string.login));
        changeHamburgerIcon(R.drawable.ic_launcher);

        isFromInbox = this.getIntent().getBooleanExtra(Constants.IS_FROM_PUSH, false);

        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        hasActivityDestroyed = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hasActivityDestroyed = true;
    }

    public boolean isActivityDestroyed() {
        return hasActivityDestroyed;
    }

    private void initViews() {

        editUserName = (EditText) findViewById(R.id.edt_user_name);
        editPassword = (EditText) findViewById(R.id.edt_password);
        checkBoxRememberMe = (CheckBox) findViewById(R.id.check_box_remember_me);


        if (!TextUtils.isEmpty(Utils.getPersistenceData(this, Constants.LOGIN_USER_NAME_PREFERENCE))) {
            editUserName.setText(Utils.getPersistenceData(this, Constants.LOGIN_USER_NAME_PREFERENCE));
        }

        if (!TextUtils.isEmpty(Utils.getPersistenceData(this, Constants.LOGIN_PASSWORD_PREFERENCE))) {
            editPassword.setText(Utils.getPersistenceData(this, Constants.LOGIN_PASSWORD_PREFERENCE));
        }

        final Button btnLogin = (Button) findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hasFieldValidated()) {
                    initAuthentication();
                    if (checkBoxRememberMe.isChecked()) {
                        saveDataInPreference();
                    } else {
                        clearPreference();
                    }

                }
            }
        });
    }

    private void callLandingActivity() {
        Intent intent = new Intent(this, LandingActivity.class);
        intent.putExtra(Constants.IS_FROM_PUSH, isFromInbox);
        startActivity(intent);
        finish();
    }

    private void clearPreference() {
        Utils.clearPreference(this, Constants.LOGIN_USER_NAME_PREFERENCE);
        Utils.clearPreference(this, Constants.LOGIN_PASSWORD_PREFERENCE);
    }

    private void initAuthentication() {
        progressDialog = Utils.getProgressDialog(this);
        progressDialog.show();
        final AuthenticationListener authenticationListener = new AuthenticationListener() {
            @Override
            public void onAuthenticationSuccess() {

                initPush();
                initCategoryFetch();

            }

            @Override
            public void onAuthenticationFailure(ErrorModel errorModel) {
                progressDialog.dismiss();
                // TODO Localization
                switch (errorModel.getErrorType()) {
                    case ERROR_TYPE_NO_NETWORK:
                        Utils.showNetworkDisableDialog(LoginActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        break;
                    case ERROR_TYPE_SERVER:
                        Utils.showGenericDialog(LoginActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        break;
                    case ERROR_TYPE_UNAUTHORIZED:
                        if (errorModel.getErrorMessage().equalsIgnoreCase(Constants.OTP_VERIFICATION_REQ)) {
                            Utils.showGenericDialog(LoginActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        } else if (errorModel.getErrorMessage().equalsIgnoreCase(Constants.USER_NOT_FOUND)) {
                            Utils.showGenericDialog(LoginActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        } else if (errorModel.getErrorMessage().equalsIgnoreCase(Constants.BAD_CREDENTIALS)) {
                            Utils.showGenericDialog(LoginActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        } else {
                            Utils.showGenericDialog(LoginActivity.this
                                    , errorModel.getErrorMessage(), isActivityDestroyed());
                        }

                        break;
                    default:
                        break;
                }

            }
        };

        AuthenticationModel authenticationModel = new AuthenticationModel();
        authenticationModel.setUserId(editUserName.getText().toString());
        authenticationModel.setPassword(editPassword.getText().toString());

        AuthenticationTask authenticationTask = new AuthenticationTask(this,
                authenticationListener, authenticationModel);
        authenticationTask.authenticate();
    }

    private void initPush() {
        if (!TextUtils.isEmpty(Utils.getPersistenceData(this, Constants.PUSH_REGISTRATION_ID_PREFERENCE))) {

            Log.d(Constants.TAG, "Preference regId: " + Utils.getPersistenceData(this, Constants.PUSH_REGISTRATION_ID_PREFERENCE));

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    initPushTokenRegistrationTask();

                }
            }, 2000);


        } else {
            pushResultReceiver = new PushResultReceiver(new Handler());
            pushResultReceiver.setReceiver(this);

            PushNotificationHelper.pushNotificationProcess(this, pushResultReceiver);
        }
    }

    private void initPushTokenRegistrationTask() {
        PushTokenRegListener pushTokenRegListener = new PushTokenRegListener() {
            @Override
            public void onPushTokenRegStart() {

            }

            @Override
            public void onPushTokenRegSuccess() {
                Log.d(Constants.TAG, "Push Reg To Server is Success");
            }

            @Override
            public void onPushTokenRegFailure(ErrorModel errorModel) {
                Log.d(Constants.TAG, "Push Reg To Server is Failure");
            }
        };

        PushTokenRegTask pushTokenRegTask = new PushTokenRegTask(this, pushTokenRegListener);
        pushTokenRegTask.registerToken(Utils.getPersistenceData(this, Constants.PUSH_REGISTRATION_ID_PREFERENCE));
    }

    private void initCategoryFetch() {

        final CategoryListener categoryListener = new CategoryListener() {
            @Override
            public void onCategoryFetchStart() {
                if (!progressDialog.isShowing()) {
                    progressDialog.show();
                }
            }

            @Override
            public void onCategoryFetchSuccess(List<CategoryModel> categoryModelList) {
                if (!categoryModelList.isEmpty()) {
                    Utils.getShopChatApplication(LoginActivity.this).setCategoryModelList(categoryModelList);
                    callLandingActivity();
                } else {
                    Utils.showGenericDialog(LoginActivity.this, R.string.no_data, isActivityDestroyed());
                }

                progressDialog.dismiss();

            }

            @Override
            public void onCategoryFetchFailure(ErrorModel errorModel) {
                progressDialog.dismiss();
                switch (errorModel.getErrorType()) {
                    case ERROR_TYPE_NO_NETWORK:
                        Utils.showNetworkDisableDialog(LoginActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        break;
                    case ERROR_TYPE_SERVER:
                        Utils.showGenericDialog(LoginActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        break;
                    default:
                        break;
                }

            }
        };

        CategoryTask categoryTask = new CategoryTask(this, categoryListener);
        categoryTask.fetchCategory();
    }


    private void callSplashActivity() {
        Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        this.finish();
    }

    private void callOTPActivity() {
        Intent intent = new Intent(this, OTPActivity.class);
        startActivity(intent);
        this.finish();
    }

    private boolean hasFieldValidated() {
        boolean hasValidated;

        if (TextUtils.isEmpty(editUserName.getText())) {
            hasValidated = false;
            Utils.showGenericDialog(this, R.string.enter_username, isActivityDestroyed());
        } else if (TextUtils.isEmpty(editPassword.getText())) {
            hasValidated = false;
            Utils.showGenericDialog(this, R.string.enter_password, isActivityDestroyed());
        } else {
            hasValidated = true;
        }

        return hasValidated;
    }


    private void saveDataInPreference() {
        Utils.setPersistenceData(this, Constants.LOGIN_USER_NAME_PREFERENCE, editUserName.getText().toString());

        Utils.setPersistenceData(this, Constants.LOGIN_PASSWORD_PREFERENCE, editPassword.getText().toString());

    }

    @Override
    public void onBackPressed() {
        Utils.showExitConfirmationDialog(this);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        String regId;
        if (resultData != null) {
            regId = resultData.getString(PushNotificationIntentService.REG_ID);

            Log.d(Constants.TAG, "regId: " + regId);

            if (!TextUtils.isEmpty(regId) && TextUtils.isEmpty(Utils.getPersistenceData(this, Constants.PUSH_REGISTRATION_ID_PREFERENCE))) {

                //Save Reg Id to preferences
                Utils.setPersistenceData(LoginActivity.this, Constants.PUSH_REGISTRATION_ID_PREFERENCE, regId);
                //initPushTokenRegistrationTask();

            }
        }
    }
}
