package com.lokalchat.retailer.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.lokalchat.retailer.R;
import com.lokalchat.retailer.crashlog.CustomExceptionHandler;
import com.lokalchat.retailer.listener.AuthenticationListener;
import com.lokalchat.retailer.listener.CategoryListener;
import com.lokalchat.retailer.models.AuthenticationModel;
import com.lokalchat.retailer.models.CategoryModel;
import com.lokalchat.retailer.models.ErrorModel;
import com.lokalchat.retailer.task.AuthenticationTask;
import com.lokalchat.retailer.task.CategoryTask;
import com.lokalchat.retailer.utils.Constants;
import com.lokalchat.retailer.utils.Utils;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class SplashActivity extends Activity {

    private final int SPLASH_TIMEOUT_DELAY = 2000;
    private String authenticatedPhoneNumber;
    private boolean isRegistered;
    private boolean hasActivityDestroyed;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Thread.setDefaultUncaughtExceptionHandler(new CustomExceptionHandler());
    }

    @Override
    protected void onResume() {
        super.onResume();

        hasActivityDestroyed = false;
        callTimeTask();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hasActivityDestroyed = true;
    }

    private void callAuthenticationFailure() {
        // TODO Handle Authentication Failure.
    }

    public boolean isActivityDestroyed() {
        return hasActivityDestroyed;
    }

    private void callTimeTask() {
        final Timer timer = new Timer();

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                callLoginActivity();
                timer.cancel();
            }
        }, SPLASH_TIMEOUT_DELAY, SPLASH_TIMEOUT_DELAY);

       /* if (TextUtils.isEmpty(Utils.getPersistenceData(this, Constants.LOGIN_USER_NAME_PREFERENCE))) {
            callLoginActivity();
        } else {
            initAuthentication();
        }*/


    }

    private void callLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        this.finish();
    }

    private void callOTPActivity() {

        Intent intent = new Intent(this, OTPActivity.class);
        startActivity(intent);

    }

    private void initCategoryFetch() {

        final CategoryListener categoryListener = new CategoryListener() {
            @Override
            public void onCategoryFetchStart() {

            }

            @Override
            public void onCategoryFetchSuccess(List<CategoryModel> categoryModelList) {
                if (!categoryModelList.isEmpty()) {
                    Utils.getShopChatApplication(SplashActivity.this).setCategoryModelList(categoryModelList);
                    callLandingActivity();
                } else {
                    Utils.showGenericDialog(SplashActivity.this, R.string.no_data, isActivityDestroyed());
                }

            }

            @Override
            public void onCategoryFetchFailure(ErrorModel errorModel) {

                switch (errorModel.getErrorType()) {
                    case ERROR_TYPE_NO_NETWORK:
                        Utils.showNetworkDisableDialog(SplashActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        break;
                    case ERROR_TYPE_SERVER:
                        Utils.showGenericDialog(SplashActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        break;
                    default:
                        break;
                }

            }
        };

        CategoryTask categoryTask = new CategoryTask(this, categoryListener);
        categoryTask.fetchCategory();
    }

    private void initAuthentication() {

        final AuthenticationListener authenticationListener = new AuthenticationListener() {
            @Override
            public void onAuthenticationSuccess() {

                initCategoryFetch();

            }

            @Override
            public void onAuthenticationFailure(ErrorModel errorModel) {
                // TODO Localization
                switch (errorModel.getErrorType()) {
                    case ERROR_TYPE_NO_NETWORK:
                        Utils.showNetworkDisableDialog(SplashActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        break;
                    case ERROR_TYPE_SERVER:
                        Utils.showGenericDialog(SplashActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        break;
                    case ERROR_TYPE_UNAUTHORIZED:
                        if (errorModel.getErrorMessage().equalsIgnoreCase(Constants.OTP_VERIFICATION_REQ)) {
                            Utils.showGenericDialog(SplashActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        } else if (errorModel.getErrorMessage().equalsIgnoreCase(Constants.USER_NOT_FOUND)) {
                            Utils.showGenericDialog(SplashActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        }

                        break;
                    default:
                        break;
                }

            }
        };

        AuthenticationModel authenticationModel = new AuthenticationModel();
        authenticationModel.setUserId(Utils.getPersistenceData(SplashActivity.this, Constants.LOGIN_USER_NAME_PREFERENCE));
        authenticationModel.setPassword("");

        AuthenticationTask authenticationTask = new AuthenticationTask(this,
                authenticationListener, authenticationModel);
        authenticationTask.authenticate();
    }


    private void callLandingActivity() {
        startActivity(LandingActivity.getIntent(this));
        finish();
    }

    private void callSignUpActivity() {
        startActivity(LoginActivity.getIntent(this));
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_splash, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
