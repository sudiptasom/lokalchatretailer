package com.lokalchat.retailer.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.lokalchat.retailer.R;
import com.lokalchat.retailer.adapters.ChatBoardListAdapter;
import com.lokalchat.retailer.adapters.FaqAdapter;
import com.lokalchat.retailer.db.DBAdapter;
import com.lokalchat.retailer.listener.ChatBoardListener;
import com.lokalchat.retailer.models.ChatBoardModel;
import com.lokalchat.retailer.models.ErrorModel;
import com.lokalchat.retailer.models.FaqModel;
import com.lokalchat.retailer.models.ProductModel;
import com.lokalchat.retailer.models.RetailerModel;
import com.lokalchat.retailer.task.ChatBoardTask;
import com.lokalchat.retailer.utils.Constants;
import com.lokalchat.retailer.utils.Utils;
import com.lokalchat.retailer.views.CustomProgress;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sudipta on 9/15/2015.
 */
public class ChatBoardActivity extends BaseActivity {
    public static String PRODUCT = "product";
    public static String PRODUCT_ID = "product_id";
    public static String QUESTION_TEXT = "question_text";
    private EditText edtChatContent;
    private List<ChatBoardModel> chatBoardListItems;
    private ChatBoardListAdapter chatBoardListAdapter;
    private ProductModel productModel;
    private static Context callingActivity;
    private String productId;
    private ListView listViewChatBoard;
    private LinearLayout llSendChat;
    private boolean isFromInbox;
    private String questionText;
    private boolean hasActivityDestroyed;

    public static Intent getIntent(Context context, ProductModel productModel) {
        Intent intent = new Intent(context, ChatBoardActivity.class);
        intent.putExtra(PRODUCT, productModel);
        callingActivity = context;

        return intent;
    }

    public static Intent getIntent(Context context, String productId, String questionText) {
        Intent intent = new Intent(context, ChatBoardActivity.class);
        intent.putExtra(PRODUCT_ID, productId);
        intent.putExtra(QUESTION_TEXT, questionText);
        callingActivity = context;

        return intent;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_board);

        productModel = this.getIntent().getParcelableExtra(PRODUCT);

        setUpSupportActionBar(false, true, "");

        chatBoardListItems = new ArrayList<ChatBoardModel>();

        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        hasActivityDestroyed = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hasActivityDestroyed = true;
    }

    public boolean isActivityDestroyed() {
        return hasActivityDestroyed;
    }

    private void initViews() {

        listViewChatBoard = (ListView) findViewById(R.id.lv_chat_board);
        final Button btnSendChat = (Button) findViewById(R.id.btn_send_chat);
        edtChatContent = (EditText) findViewById(R.id.edt_chat_content);
        llSendChat = (LinearLayout) findViewById(R.id.ll_send_chat);
        btnSendChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(edtChatContent.getText())) {
                    sendChat();
                } else {
                    Utils.showGenericDialog(ChatBoardActivity.this, R.string.enter_text, isActivityDestroyed());
                }

            }
        });

        if (callingActivity instanceof LandingActivity) {
            this.productId = this.getIntent().getStringExtra(PRODUCT_ID);
            this.questionText = this.getIntent().getStringExtra(QUESTION_TEXT);
            createChatBoardRowFromInbox();
            isFromInbox = true;
        } else {
            isFromInbox = false;
        }

        chatBoardListAdapter = new ChatBoardListAdapter(this, chatBoardListItems, isFromInbox);
        listViewChatBoard.setAdapter(chatBoardListAdapter);

        final Button btnFaq = (Button) findViewById(R.id.btn_faq);
        btnFaq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFaqDialog();
            }
        });

    }

    private void createChatBoardRowFromInbox() {

       /* MultiMap<String, ProductModel> chatMap = Utils.getShopChatApplication(this).getChatMap();

        List<ProductModel> chatList = (List<ProductModel>) chatMap.get(productId);*/


        List<ProductModel> chatList = Utils.getShopChatApplication(this).getChatList();

        for (ProductModel productModel : chatList) {
            if (productModel.getConsumerChatContent().equalsIgnoreCase(questionText)) {
                this.productModel = productModel;
            }
        }


        ChatBoardModel chatBoardModel = new ChatBoardModel(productModel.getConsumerChatContent(), true, 0, null, productModel, productModel.getLastModifiedDate());
        chatBoardListItems.add(chatBoardModel);

    }

    private void showFaqDialog() {
        Dialog faqDialog = new Dialog(this);
        faqDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        faqDialog.setContentView(R.layout.dialog_faq);
        faqDialog.show();

        final ListView lvFaq = (ListView) faqDialog.findViewById(R.id.lv_faq);

        List<FaqModel> listFaq = new ArrayList<FaqModel>();
        listFaq.add(new FaqModel(""));
        listFaq.add(new FaqModel(""));
        listFaq.add(new FaqModel(""));
        listFaq.add(new FaqModel(""));
        listFaq.add(new FaqModel(""));
        listFaq.add(new FaqModel(""));


        FaqAdapter faqAdapter = new FaqAdapter(this, listFaq);
        lvFaq.setAdapter(faqAdapter);

    }

    private void sendChat() {

        Utils.getShopChatApplication(this).setChatBoardModels(createChatBoardRow());

        final CustomProgress progressDialog = Utils.getProgressDialog(this);
        ChatBoardListener chatBoardListener = new ChatBoardListener() {
            @Override
            public void onSendChatStart() {
                progressDialog.show();
            }

            @Override
            public void onSendChatSuccess() {
                progressDialog.dismiss();
                deleteRepliedChat();
                Utils.getShopChatApplication(ChatBoardActivity.this).getChatDisplayModels().clear();
                chatBoardListAdapter.notifyDataSetChanged();
                callLandingActivity();
            }

            @Override
            public void onSendChatFailure(ErrorModel errorModel) {
                progressDialog.dismiss();
                switch (errorModel.getErrorType()) {
                    case ERROR_TYPE_NO_NETWORK:
                        Utils.showNetworkDisableDialog(ChatBoardActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        break;
                    case ERROR_TYPE_SERVER:
                        Utils.showGenericDialog(ChatBoardActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        break;
                    default:
                        rollBackChat();
                        break;
                }

            }
        };

        ChatBoardTask chatBoardTask = new ChatBoardTask(this, chatBoardListener);
        chatBoardTask.sendChat();
    }

    private void deleteRepliedChat() {
        DBAdapter.deleteRepliedChatFromDB(this, productModel.getConsumerChatId());
        int totalRecords = DBAdapter.getRecordsCount(this);
        Log.d(Constants.TAG, "Total Number of Database records after delete:" + totalRecords);
    }

    private void callLandingActivity() {
        startActivity(LandingActivity.getIntent(this, ChatBoardActivity.class.toString(), productModel));
    }

    private void rollBackChat() {
        // Last item of List
        chatBoardListItems.remove(chatBoardListItems.size() - 1);
        Utils.getShopChatApplication(ChatBoardActivity.this).getChatBoardModels().clear();
        chatBoardListAdapter.notifyDataSetChanged();

        // TODO Localization
        Toast.makeText(ChatBoardActivity.this, "Sending failed!", Toast.LENGTH_SHORT).show();
    }

    private List<ChatBoardModel> createChatBoardRow() {
        RetailerModel retailerModel = new RetailerModel();
        ChatBoardModel chatBoardModel = new ChatBoardModel(edtChatContent.getText().toString(), false, 0, null, productModel, null);
        chatBoardListItems.add(chatBoardModel);
        edtChatContent.setText("");

        return chatBoardListItems;
    }


}
