package com.lokalchat.retailer.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.lokalchat.retailer.R;
import com.lokalchat.retailer.fragments.ItemListFragment;
import com.lokalchat.retailer.fragments.SearchFragment;


/**
 * Created by Sudipta on 8/9/2015.
 */
public class ItemListActivity extends BaseActivity {

    private FragmentManager fragmentManager;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);
        setUpSupportActionBar(false, true, "Item Name");

        initViews();
    }

    private void initViews() {

        fragmentManager = getFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.container);
        if (fragment == null) {
            fragment = new ItemListFragment();
            fragmentManager.beginTransaction()
                    .add(R.id.container, fragment)
                    .commit();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                replaceWithSearchFragment();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void replaceWithSearchFragment() {

        Fragment searchFragment = new SearchFragment().newInstance();
        ((SearchFragment) searchFragment).setCancelClickListener(cancelClickListener);
        fragmentManager.beginTransaction()
                .replace(R.id.container, searchFragment)
                .commit();
    }

    private SearchFragment.CancelClickListener cancelClickListener = new SearchFragment.CancelClickListener() {
        @Override
        public void onCancelClick() {
            Fragment itemListFragment = new ItemListFragment();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, itemListFragment)
                    .commit();
        }
    };

}
