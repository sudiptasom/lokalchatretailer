package com.lokalchat.retailer.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;

import com.lokalchat.retailer.R;
import com.lokalchat.retailer.adapters.HomePageFragmentAdapter;
import com.lokalchat.retailer.fragments.ChatFragment;
import com.lokalchat.retailer.fragments.HomeFragment;
import com.lokalchat.retailer.fragments.ProfileFragment;
import com.lokalchat.retailer.fragments.ShopChatNavigationFragment;
import com.lokalchat.retailer.listener.ChatListener;
import com.lokalchat.retailer.listener.CityListener;
import com.lokalchat.retailer.listener.NewMessageCountListener;
import com.lokalchat.retailer.models.ChatDisplayModel;
import com.lokalchat.retailer.models.CityModel;
import com.lokalchat.retailer.models.ErrorModel;
import com.lokalchat.retailer.models.ProductModel;
import com.lokalchat.retailer.models.RetailerModel;
import com.lokalchat.retailer.task.ChatTask;
import com.lokalchat.retailer.task.CityTask;
import com.lokalchat.retailer.task.NewMessageCountTask;
import com.lokalchat.retailer.utils.Constants;
import com.lokalchat.retailer.utils.Utils;
import com.lokalchat.retailer.views.ActionBarHome;
import com.lokalchat.retailer.views.CustomProgress;
import com.lokalchat.retailer.views.SlidingTabLayout;

import org.apache.commons.collections4.MultiMap;
import org.apache.commons.collections4.map.MultiValueMap;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Sudipta on 8/7/2015.
 */
public class LandingActivity extends BaseActivity implements ActionBarHome.OnActionBarItemClickListener {

    public static String CALLING_ACTIVITY = "calling_activity";
    public static String PRODUCT = "product";
    public static String PRODUCT_ID = "product_id";
    public static final int REFRESH_TIME = 60000;
    private SlidingTabLayout slidingTabLayout;
    private ViewPager viewPager;
    private ShopChatNavigationFragment mNavigationDrawerFragment;
    private HomePageFragmentAdapter adapter;
    private SaveClickListener saveClickListener;
    private ActionBarHome actionBarHome;
    private CustomProgress progressDialog;
    private ChatAdapterListener chatAdapterListener;
    private ProductModel productModel;
    private boolean hasActivityDestroyed;
    private boolean isChatFirstTimeLoad = true;
    private String callingActivity;
    private Timer timer;
    private boolean isFromInbox;


    public static Intent getIntent(Context context, String callingActivity, ProductModel productModel) {
        Intent intent = new Intent(context, LandingActivity.class);
        intent.putExtra(CALLING_ACTIVITY, callingActivity);
        intent.putExtra(PRODUCT, productModel);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }

    public static Intent getIntent(Context context) {
        Intent intent = new Intent(context, LandingActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        setUpSupportActionBar(true, false, "Home");

        mNavigationDrawerFragment = (ShopChatNavigationFragment) getFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));
        mNavigationDrawerFragment.closeNavigationDrawer();

        isFromInbox = this.getIntent().getBooleanExtra(Constants.IS_FROM_PUSH, false);

        initViews();

        if (callingActivity == null) {
            initChatFetchTask("0", false, false, 5);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        actionBarHome = getActionBarHome();
        hasActivityDestroyed = false;

        fetchChatPeriodically();
        setUpSelectedLocation();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (timer != null) {
            timer.cancel();
        }
    }

    private void fetchChatPeriodically() {
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        initNewMessageTask();
                    }
                });
            }
        }, 0, REFRESH_TIME);
    }

    private void initNewMessageTask() {

        NewMessageCountListener newMessageCountListener = new NewMessageCountListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(String response) {
                if (response.equalsIgnoreCase("0")) {

                } else {
                    if (viewPager.getCurrentItem() == 0) {
                        initChatFetchTask("0", false, true, 5);
                    } else {
                        initChatFetchTask("0", false, false, 5);
                    }
                }
            }

            @Override
            public void onFailure(ErrorModel errorModel) {
                switch (errorModel.getErrorType()) {
                    case ERROR_TYPE_NO_NETWORK:
                        Utils.showNetworkDisableDialog(LandingActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        break;
                    case ERROR_TYPE_SERVER:
                        Utils.showGenericDialog(LandingActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        break;
                    default:

                        break;
                }
            }
        };

        NewMessageCountTask newMessageCountTask = new NewMessageCountTask(this, newMessageCountListener);
        newMessageCountTask.fetchNewMessage();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        hasActivityDestroyed = true;
        isChatFirstTimeLoad = false;
    }

    public boolean isActivityDestroyed() {
        return hasActivityDestroyed;
    }

    private List<ChatDisplayModel> createChatDisplayList(MultiMap<String, ProductModel> chatMap) {
        List<ChatDisplayModel> chatDisplayModelList = new ArrayList<ChatDisplayModel>();

        Iterator iterator = chatMap.entrySet().iterator();

        while (iterator.hasNext()) {
            MultiValueMap.Entry entry = (MultiValueMap.Entry) iterator.next();

            ArrayList<ProductModel> productList = (ArrayList) entry.getValue();
            ChatDisplayModel chatDisplayModel = new ChatDisplayModel();
            StringBuilder stringBuilder = new StringBuilder();
            for (ProductModel productModel : productList) {
                chatDisplayModel.setProductId(productModel.getProductId());
                chatDisplayModel.setProductName(productModel.getProductName());
                for (RetailerModel retailerModel : productModel.getRetailerModels()) {
                    stringBuilder.append(retailerModel.getRetailerChatContent());
                    stringBuilder.append(" ");
                }
            }
            chatDisplayModel.setRetailerConsolidatedChatContent(stringBuilder.toString());
            chatDisplayModelList.add(chatDisplayModel);

        }

        return chatDisplayModelList;
    }


    private void setUpSelectedLocation() {
        final String selectedCity = Utils.getPersistenceData(this, Constants.CITY_PREFERENCE);
        final String selectedLocation = Utils.getPersistenceData(this, Constants.LOCATION_PREFERENCE);

        // TODO Location will be fetched from server.
        if (TextUtils.isEmpty(selectedCity) || TextUtils.isEmpty(selectedLocation)) {
            //showPickCityDialog();
        } else {
            getActionBarHome().setSelectedState(selectedCity);
            getActionBarHome().setSelectedLocation(selectedLocation);
        }
    }

    private void initViews() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            adapter = new HomePageFragmentAdapter(getFragmentManager());
        } else {
            adapter = new HomePageFragmentAdapter(getFragmentManager());
            adapter.setReuseFragments(false);
        }

        ChatFragment chatFragment = new ChatFragment();
        chatFragment.setPageListener(pageListener);

        adapter.addFragment(getString(R.string.chat_fragment), chatFragment);
        adapter.addFragment(getString(R.string.home_fragment), new HomeFragment());
        adapter.addFragment(getString(R.string.profile_fragment), new ProfileFragment());

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        slidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);

        viewPager.setAdapter(adapter);
        slidingTabLayout.setViewPager(viewPager);

        callingActivity = this.getIntent().getStringExtra(CALLING_ACTIVITY);
        productModel = this.getIntent().getParcelableExtra(PRODUCT);
        if (callingActivity != null && callingActivity.equalsIgnoreCase(ChatBoardActivity.class.toString()) && productModel != null) {
            viewPager.setCurrentItem(0);
            initChatFetchTask("0", false, false, 5);
        }

        slidingTabLayout.addPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (adapter.getItem(position) instanceof ProfileFragment) {
                    getActionBarHome().setVisibleFragment(new ProfileFragment());
                } else {
                    changeActionBarMultipleMenuIcon(R.drawable.ic_share);
                    getActionBarHome().showActionBarMultipleMenuIcon();
                    getActionBarHome().setVisibleFragment(null);
                }


                Utils.hideKeyBoard(LandingActivity.this);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    ChatFragment.PageListener pageListener = new ChatFragment.PageListener() {
        @Override
        public void onLoadMore(int limit) {
            int currentPageNumber = Utils.getShopChatApplication(LandingActivity.this).getCurrentChatPageNumber() + 1;
            initChatFetchTask(String.valueOf(currentPageNumber), true, false, limit);
        }
    };

    @Override
    public void onSelectedLocationClick() {
        //initCityFetch();
        startActivity(CityActivity.getIntent(LandingActivity.this));
    }

    private void initCityFetch() {

        progressDialog = Utils.getProgressDialog(this);
        CityListener cityListener = new CityListener() {
            @Override
            public void onCityFetchedStart() {
                progressDialog.show();
            }

            @Override
            public void onCityFetchSuccess(List<CityModel> cityModelList) {
                progressDialog.dismiss();
                Utils.getShopChatApplication(LandingActivity.this).setCityModelList(cityModelList);
                startActivity(CityActivity.getIntent(LandingActivity.this));
            }

            @Override
            public void onCityFetchFailure(ErrorModel errorModel) {
                progressDialog.dismiss();

                switch (errorModel.getErrorType()) {
                    case ERROR_TYPE_NO_NETWORK:
                        Utils.showNetworkDisableDialog(LandingActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        break;
                    case ERROR_TYPE_SERVER:
                        Utils.showGenericDialog(LandingActivity.this, errorModel.getErrorMessage(), isActivityDestroyed());
                        break;
                    default:
                        break;
                }

            }
        };

        CityTask cityTask = new CityTask(this, cityListener);
        cityTask.fetchCity();
    }

    @Override
    public void onMultipleActionMenuClick() {
        Utils.callShareIntent(this, getString(R.string.app_name), Utils.getSharableContent(this));
    }

    @Override
    public void onMenuIconClick() {
        mNavigationDrawerFragment.openNavigationDrawer();
    }

    public void setSaveClickListener(SaveClickListener listener) {
        this.saveClickListener = listener;
    }

    public interface SaveClickListener {
        void onSaveClick();
    }

    public void setChatAdapterListener(ChatAdapterListener chatAdapterListener) {
        this.chatAdapterListener = chatAdapterListener;
    }

    public interface ChatAdapterListener {
        void onChatFetchSuccess(List<ChatDisplayModel> chatDisplayModels);

        void onChatFetchFailure(ErrorModel errorModel);
    }

    @Override
    public void onBackPressed() {
        if (mNavigationDrawerFragment.isVisible()) {
            mNavigationDrawerFragment.closeNavigationDrawer();
        } else {
            Utils.showExitConfirmationDialog(this);
        }
    }

    public ActionBarHome getActivityActionBar() {
        return actionBarHome;
    }

    public void showPickCityDialog() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.please_pick_location)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        closeActivity();
                    }
                })
                .show();
    }


    private void closeActivity() {
        this.finish();
    }

    public ProductModel getProductModel() {
        return this.productModel;
    }

    public CustomProgress getProgressDialog() {
        return progressDialog;
    }

    private void initChatFetchTask(final String pageCounter, final boolean isLoadMore, final boolean isFromAlertDialog, int limit) {

        progressDialog = Utils.getProgressDialog(this);
        ChatListener chatListener = new ChatListener() {
            @Override
            public void onChatFetchStart() {
                // progressDialog.show();
            }

            @Override
            public void onChatFetchSuccess(List<ProductModel> chatList) {
                //progressDialog.dismiss();
                isChatFirstTimeLoad = false;
                List<ChatDisplayModel> chatDisplayModelList = createInboxDisplayList(chatList);

                if (!isLoadMore) {
                    if (isFromAlertDialog) {
                        // TODO Localization
                        //showNewMessageAvailableDialog(LandingActivity.this, "There is a new message available, would you like to read it?", chatList, chatDisplayModelList);
                        setChatData(chatList, chatDisplayModelList);
                        chatAdapterListener.onChatFetchSuccess(chatDisplayModelList);
                    } else {
                        setChatData(chatList, chatDisplayModelList);
                        chatAdapterListener.onChatFetchSuccess(chatDisplayModelList);
                    }

                } else {
                    setChatData(chatList, chatDisplayModelList);
                    chatAdapterListener.onChatFetchSuccess(chatDisplayModelList);

                }

            }

            @Override
            public void onChatFetchFailure(ErrorModel errorModel) {
                // progressDialog.dismiss();
                chatAdapterListener.onChatFetchFailure(errorModel);
            }
        };

        ChatTask chatTask = new ChatTask(this, chatListener, pageCounter, isLoadMore, isChatFirstTimeLoad, isFromAlertDialog, limit);
        chatTask.fetchChat();
    }

    private List<ChatDisplayModel> createInboxDisplayList(List<ProductModel> chatList) {
        List<ChatDisplayModel> chatDisplayModelList = new ArrayList<ChatDisplayModel>();


        for (ProductModel productModel : chatList) {
            ChatDisplayModel chatDisplayModel = new ChatDisplayModel();
            chatDisplayModel.setProductId(productModel.getProductId());
            chatDisplayModel.setProductName(productModel.getProductName());
            chatDisplayModel.setIsRead(productModel.isRead());
            chatDisplayModel.setChatId(productModel.getConsumerChatId());
            StringBuilder stringBuilder = new StringBuilder();
            for (RetailerModel retailerModel : productModel.getRetailerModels()) {
                if (!retailerModel.getRetailerChatContent().isEmpty()) {
                    stringBuilder.append(retailerModel.getRetailerChatContent());
                    stringBuilder.append(" ");
                }
            }
            chatDisplayModel.setConsumerChatContent(productModel.getConsumerChatContent());
            chatDisplayModel.setRetailerConsolidatedChatContent(stringBuilder.toString());
            chatDisplayModelList.add(chatDisplayModel);
        }


        return chatDisplayModelList;
    }

    public void showNewMessageAvailableDialog(final Context context, String msg, final List<ProductModel> chatList, final List<ChatDisplayModel> chatDisplayModelList) {
        new AlertDialog.Builder(context)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        setChatData(chatList, chatDisplayModelList);
                        chatAdapterListener.onChatFetchSuccess(chatDisplayModelList);
                    }
                })
                .show();
    }


    private void setChatData(List<ProductModel> chatList, List<ChatDisplayModel> chatDisplayModelList) {
        Utils.getShopChatApplication(LandingActivity.this).setChatDisplayModels(chatDisplayModelList);
        Utils.getShopChatApplication(LandingActivity.this).setChatList(chatList);
    }

}
