package com.lokalchat.retailer.listener;


import com.lokalchat.retailer.models.ErrorModel;
import com.lokalchat.retailer.models.RetailerModel;

import java.util.List;

/**
 * Created by Sudipta on 9/26/2015.
 */
public abstract class RetailerListener {

    public abstract void onRetailerFetchStart();

    public abstract void onRetailerFetchSuccess(List<RetailerModel> retailerModels);

    public abstract void onRetailerFetchFailure(ErrorModel errorModel);
}
