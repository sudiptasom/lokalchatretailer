package com.lokalchat.retailer.listener;


import com.lokalchat.retailer.models.CityModel;
import com.lokalchat.retailer.models.ErrorModel;

import java.util.List;

/**
 * Created by Sudipta on 9/25/2015.
 */
public abstract class CityListener {

    public abstract void onCityFetchedStart();

    public abstract void onCityFetchSuccess(List<CityModel> cityModelList);

    public abstract void onCityFetchFailure(ErrorModel errorModel);

}
