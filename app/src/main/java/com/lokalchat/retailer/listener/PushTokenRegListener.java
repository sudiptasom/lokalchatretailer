package com.lokalchat.retailer.listener;

import com.lokalchat.retailer.models.ErrorModel;

/**
 * Created by Sudipta on 4/9/2016.
 */
public interface PushTokenRegListener {
    void onPushTokenRegStart();

    void onPushTokenRegSuccess();

    void onPushTokenRegFailure(ErrorModel errorModel);
}
