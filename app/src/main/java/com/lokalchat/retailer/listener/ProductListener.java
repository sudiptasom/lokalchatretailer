package com.lokalchat.retailer.listener;


import com.lokalchat.retailer.models.ErrorModel;
import com.lokalchat.retailer.models.ProductModel;

import java.util.List;

/**
 * Created by Sudipta on 9/26/2015.
 */
public abstract class ProductListener {

    public abstract void onProductFetchStart();

    public abstract void onProductFetchSuccess(List<ProductModel> productModels);

    public abstract void onProductFetchFailure(ErrorModel errorModel);
}
