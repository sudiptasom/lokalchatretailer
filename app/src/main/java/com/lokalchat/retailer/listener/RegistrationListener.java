package com.lokalchat.retailer.listener;


import com.lokalchat.retailer.models.ErrorModel;

/**
 * Created by Sudipta on 10/7/2015.
 */
public abstract class RegistrationListener {
    public abstract void onRegistrationStart();

    public abstract void onRegistrationSuccess();

    public abstract void onRegistrationFailure(ErrorModel errorModel);


}
