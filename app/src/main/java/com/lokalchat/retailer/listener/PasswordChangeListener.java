package com.lokalchat.retailer.listener;


import com.lokalchat.retailer.models.ErrorModel;

/**
 * Created by Sudipta on 1/30/2016.
 */
public interface PasswordChangeListener {
    void onPasswordChangeFailure(ErrorModel errorModel);

    void onPasswordChangeStart();

    void onPasswordChangeSuccess();
}
