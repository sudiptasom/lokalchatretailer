package com.lokalchat.retailer.listener;



import com.lokalchat.retailer.models.ErrorModel;
import com.lokalchat.retailer.models.LocationModel;

import java.util.List;

/**
 * Created by Sudipta on 9/26/2015.
 */
public abstract class CityLocationListener {

    public abstract void onCityLocationFetchStart();

    public abstract void onCityLocationFetchSuccess(List<LocationModel> locationModels);

    public abstract void onCityLocationFetchFailure(ErrorModel errorModel);
}
