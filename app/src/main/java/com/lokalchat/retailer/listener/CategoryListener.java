package com.lokalchat.retailer.listener;


import com.lokalchat.retailer.models.CategoryModel;
import com.lokalchat.retailer.models.ErrorModel;

import java.util.List;

/**
 * Created by Sudipta on 9/24/2015.
 */
public abstract class CategoryListener {
    public abstract void onCategoryFetchStart();

    public abstract void onCategoryFetchSuccess(List<CategoryModel> categoryModelList);

    public abstract void onCategoryFetchFailure(ErrorModel errorModel);
}
