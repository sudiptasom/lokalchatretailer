package com.lokalchat.retailer.task;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.lokalchat.retailer.R;
import com.lokalchat.retailer.ShopChatApplication;
import com.lokalchat.retailer.listener.CategoryListener;
import com.lokalchat.retailer.models.CategoryModel;
import com.lokalchat.retailer.models.CityModel;
import com.lokalchat.retailer.models.ErrorModel;
import com.lokalchat.retailer.models.LocationModel;
import com.lokalchat.retailer.models.LoginModel;
import com.lokalchat.retailer.models.ProductModel;
import com.lokalchat.retailer.network.HttpConnection;
import com.lokalchat.retailer.utils.Constants;
import com.lokalchat.retailer.utils.Utils;

import org.apache.commons.collections4.MultiMap;
import org.apache.commons.collections4.map.MultiValueMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Sudipta on 9/24/2015.
 */
public class CategoryTask {

    private Context context;
    private HttpConnection httpConnect;
    private CategoryListener categoryListener;

    public CategoryTask(Context context, CategoryListener categoryListener) {
        this.context = context;
        this.categoryListener = categoryListener;

    }

    public void fetchCategory() {
        if (!Utils.isConnectionPossible(this.context)) {
            ErrorModel errorModel = new ErrorModel();
            errorModel.setErrorType(ErrorModel.Error.ERROR_TYPE_NO_NETWORK);
            // TODO used actual text
            errorModel.setErrorMessage(this.context.getResources().getString(R.string.error_no_network));
            categoryListener.onCategoryFetchFailure(errorModel);

        } else {
            initNetworkTask();
        }
    }

    private void initNetworkTask() {

        Handler networkHandler = new Handler() {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case HttpConnection.DID_START:
                        categoryListener.onCategoryFetchStart();
                        break;
                    case HttpConnection.DID_SUCCEED:
                        String jsonResponse = (String) message.obj;
                        categoryListener.onCategoryFetchSuccess(getCategoryList(jsonResponse));
                        break;
                    case HttpConnection.DID_UNSUCCESS:
                        ErrorModel errorModel = new ErrorModel();
                        errorModel.setErrorType(ErrorModel.Error.ERROR_TYPE_SERVER);
                        // TODO used actual text
                        errorModel.setErrorMessage(context.getResources().getString(R.string.error_server));
                        categoryListener.onCategoryFetchFailure(errorModel);
                        break;
                    case HttpConnection.DID_ERROR:
                        ErrorModel errorModel2 = new ErrorModel();
                        errorModel2.setErrorType(ErrorModel.Error.ERROR_TYPE_SERVER);
                        // TODO used actual text
                        errorModel2.setErrorMessage(context.getResources().getString(R.string.error_server));
                        categoryListener.onCategoryFetchFailure(errorModel2);
                        break;
                    default:
                        break;
                }
            }
        };

        httpConnect = new HttpConnection(networkHandler);

        final String categoryUrl = Constants.BASE_URL + Constants.RETAILER_DETAIL_URL;
        LoginModel loginModel = ((ShopChatApplication) context.getApplicationContext()).getLoginModel();
        httpConnect.post(categoryUrl, null, Constants.BLANK_TEXT, HttpConnection.REQUEST_COMMON, loginModel);
    }

    private List<CategoryModel> getCategoryList(String response) {

        MultiMap<String, CategoryModel> categoryMap = new MultiValueMap<String, CategoryModel>();
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONObject userObject = jsonObject.getJSONObject("user");
            Utils.setPersistenceData(context, Constants.REGISTERED_EMAIL_PREFERENCE, userObject.getString("email"));
            Utils.setPersistenceData(context, Constants.REGISTERED_PHONE_PREFERENCE, userObject.getString("mobile"));

            JSONObject addressObject = jsonObject.getJSONObject("address");

            List<CityModel> cityModelList = new ArrayList<CityModel>();
            CityModel cityModel = new CityModel();
            cityModel.setCityName(addressObject.getString("city"));

            String cityLocation = addressObject.getString("locality");
            List<LocationModel> locationModels = new ArrayList<LocationModel>();
            if (cityLocation.contains(",")) {
                String[] locationArray = cityLocation.split(",");

                for (int ii = 0; ii < locationArray.length; ii++) {
                    LocationModel locationModel = new LocationModel();
                    locationModel.setLocationName(locationArray[ii]);
                    locationModels.add(locationModel);
                }
            } else {
                LocationModel locationModel = new LocationModel();
                locationModel.setLocationName(cityLocation);
                locationModels.add(locationModel);
            }

            cityModel.setLocationModelList(locationModels);
            cityModelList.add(cityModel);

            Utils.getShopChatApplication((Activity) context).setCityModelList(cityModelList);
          /*  Utils.setPersistenceData(context, Constants.LOCATION_PREFERENCE, addressObject.getString("locality"));
            Utils.setPersistenceData(context, Constants.CITY_PREFERENCE, addressObject.getString("city"));*/
            Utils.setPersistenceData(context, Constants.REGISTERED_NAME_PREFERENCE, jsonObject.getString("shopName"));


            JSONArray jsonArray = jsonObject.getJSONArray("products");
            for (int ii = 0; ii < jsonArray.length(); ii++) {
                CategoryModel categoryModel = new CategoryModel();
                JSONObject jObject = jsonArray.getJSONObject(ii);
                categoryModel.setItemName(jObject.getString("category"));
                categoryModel.setItemPictureUrl(jObject.getString("imageurl"));

                List<ProductModel> productModelList = new ArrayList<ProductModel>();
                ProductModel productModel = new ProductModel();
                productModel.setProductName(jObject.getString("productName"));
                productModelList.add(productModel);
                categoryModel.setProductModels(productModelList);
                categoryMap.put(categoryModel.getItemName(), categoryModel);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return createCategoryListFromMap(categoryMap);
    }

    private List<CategoryModel> createCategoryListFromMap(MultiMap<String, CategoryModel> categoryMap) {
        List<CategoryModel> categoryModelList = new ArrayList<CategoryModel>();

        Iterator iterator = categoryMap.entrySet().iterator();
        while (iterator.hasNext()) {
            MultiValueMap.Entry entry = (MultiValueMap.Entry) iterator.next();
            List<CategoryModel> tempCategoryList = (ArrayList) entry.getValue();
            CategoryModel tempCategoryModel = new CategoryModel(tempCategoryList.get(0).getItemName(), tempCategoryList.get(0).getItemPictureUrl());
            List<ProductModel> productModelList = new ArrayList<ProductModel>();
            for (CategoryModel categoryModel : tempCategoryList) {
                productModelList.addAll(categoryModel.getProductModels());
            }

            tempCategoryModel.setProductModels(productModelList);
            categoryModelList.add(tempCategoryModel);
        }

        return categoryModelList;

    }
}
