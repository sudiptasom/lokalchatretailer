package com.lokalchat.retailer.task;

import android.content.Context;
import android.os.Handler;
import android.os.Message;


import com.lokalchat.retailer.R;
import com.lokalchat.retailer.listener.PasswordChangeListener;
import com.lokalchat.retailer.models.ErrorModel;
import com.lokalchat.retailer.models.LoginModel;
import com.lokalchat.retailer.network.HttpConnection;
import com.lokalchat.retailer.utils.Constants;
import com.lokalchat.retailer.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Sudipta on 1/30/2016.
 */
public class PasswordChangeTask {
    private Context context;
    private HttpConnection httpConnect;
    private PasswordChangeListener passwordChangeListener;
    private String oldPassword;
    private String newPassword;
    private String username;

    public PasswordChangeTask(Context context, PasswordChangeListener passwordChangeListener, String username,
                              String oldPassword, String newPassword) {
        this.context = context;
        this.passwordChangeListener = passwordChangeListener;
        if (oldPassword == null) {
            this.oldPassword = "";
        } else {
            this.oldPassword = oldPassword;
        }
        this.newPassword = newPassword;
        this.username = username;
    }

    public void initPasswordChange() {
        if (!Utils.isConnectionPossible(this.context)) {
            ErrorModel errorModel = new ErrorModel();
            errorModel.setErrorType(ErrorModel.Error.ERROR_TYPE_NO_NETWORK);
            // TODO used actual text
            errorModel.setErrorMessage(context.getResources().getString(R.string.error_no_network));
            passwordChangeListener.onPasswordChangeFailure(errorModel);

        } else {
            initNetworkTask();
        }
    }

    private void initNetworkTask() {

        Handler networkHandler = new Handler() {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case HttpConnection.DID_START:
                        passwordChangeListener.onPasswordChangeStart();
                        break;
                    case HttpConnection.DID_SUCCEED:
                        String jsonResponse = (String) message.obj;
                        passwordChangeListener.onPasswordChangeSuccess();
                        break;
                    case HttpConnection.DID_UNSUCCESS:
                        ErrorModel errorModel = new ErrorModel();
                        errorModel.setErrorType(ErrorModel.Error.ERROR_TYPE_BAD_REQUEST);
                        // TODO used actual text
                        errorModel.setErrorMessage(context.getString(R.string.password_change_error));
                        passwordChangeListener.onPasswordChangeFailure(errorModel);
                        break;
                    case HttpConnection.DID_ERROR:
                        ErrorModel errorModel2 = new ErrorModel();
                        errorModel2.setErrorType(ErrorModel.Error.ERROR_TYPE_SERVER);
                        // TODO used actual text
                        errorModel2.setErrorMessage(context.getResources().getString(R.string.error_server));
                        passwordChangeListener.onPasswordChangeFailure(errorModel2);
                        break;
                    default:
                        break;
                }
            }
        };

        httpConnect = new HttpConnection(networkHandler);

        final String changePasswordUrl = Constants.BASE_URL + Constants.CHANG_PASSWORD_URL;
        httpConnect.post(changePasswordUrl, null, getRequest(), HttpConnection.REQUEST_COMMON, new LoginModel());
    }

    private String getMessage(String jsonUnsuccessResponse) {
        String response = null;
        try {
            JSONObject jsonObject = new JSONObject(jsonUnsuccessResponse);
            response = jsonObject.getString("error");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return response;
    }

    private String getRequest() {
        String requestBody = null;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("username", this.username);
            jsonObject.put("oldpassword", this.oldPassword);
            jsonObject.put("newpassword", this.newPassword);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        requestBody = jsonObject.toString();
        return requestBody;
    }
}
