package com.lokalchat.retailer.task;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.lokalchat.retailer.R;
import com.lokalchat.retailer.ShopChatApplication;
import com.lokalchat.retailer.listener.PushTokenRegListener;
import com.lokalchat.retailer.models.ErrorModel;
import com.lokalchat.retailer.models.LoginModel;
import com.lokalchat.retailer.network.HttpConnection;
import com.lokalchat.retailer.utils.Constants;
import com.lokalchat.retailer.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Sudipta on 4/9/2016.
 */
public class PushTokenRegTask {
    private final Context context;
    private final PushTokenRegListener pushTokenRegListener;
    private HttpConnection httpConnect;

    public PushTokenRegTask(Context context, PushTokenRegListener pushTokenRegListener) {
        this.context = context;
        this.pushTokenRegListener = pushTokenRegListener;
    }

    public void registerToken(String pushRegToken) {
        if (!Utils.isConnectionPossible(this.context)) {
            ErrorModel errorModel = new ErrorModel();
            errorModel.setErrorType(ErrorModel.Error.ERROR_TYPE_NO_NETWORK);
            // TODO used actual text
            errorModel.setErrorMessage(this.context.getResources().getString(R.string.error_no_network));
            pushTokenRegListener.onPushTokenRegFailure(errorModel);

        } else {
            initNetworkTask(pushRegToken);
        }
    }

    private void initNetworkTask(String pushRegToken) {
        Handler networkHandler = new Handler() {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case HttpConnection.DID_START:
                        pushTokenRegListener.onPushTokenRegStart();
                        break;
                    case HttpConnection.DID_SUCCEED:
                        String jsonResponse = (String) message.obj;
                        pushTokenRegListener.onPushTokenRegSuccess();
                        break;
                    case HttpConnection.DID_UNSUCCESS:
                        ErrorModel errorModel = new ErrorModel();
                        errorModel.setErrorType(ErrorModel.Error.ERROR_TYPE_SERVER);
                        // TODO used actual text
                        errorModel.setErrorMessage(context.getResources().getString(R.string.error_server));
                        pushTokenRegListener.onPushTokenRegFailure(errorModel);

                        break;
                    case HttpConnection.DID_ERROR:
                        ErrorModel errorModel2 = new ErrorModel();
                        errorModel2.setErrorType(ErrorModel.Error.ERROR_TYPE_SERVER);
                        // TODO used actual text
                        errorModel2.setErrorMessage(context.getResources().getString(R.string.error_server));
                        pushTokenRegListener.onPushTokenRegFailure(errorModel2);
                        break;
                    default:
                        break;
                }
            }
        };

        httpConnect = new HttpConnection(networkHandler);

        final String pushTokenRegUrl = Constants.BASE_URL + Constants.PUSH_REGISTRATION_URL;
        LoginModel loginModel = ((ShopChatApplication) context.getApplicationContext()).getLoginModel();
        httpConnect.post(pushTokenRegUrl, null, getRequestBody(pushRegToken), HttpConnection.REQUEST_COMMON, loginModel);
    }

    private String getRequestBody(String pushRegToken) {
        String requestBody = null;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("clientoken", pushRegToken);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        requestBody = jsonObject.toString();
        return requestBody;
    }
}
