package com.lokalchat.retailer.task;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.lokalchat.retailer.R;
import com.lokalchat.retailer.ShopChatApplication;
import com.lokalchat.retailer.db.DBAdapter;
import com.lokalchat.retailer.listener.ChatListener;
import com.lokalchat.retailer.models.ErrorModel;
import com.lokalchat.retailer.models.LoginModel;
import com.lokalchat.retailer.models.ProductModel;
import com.lokalchat.retailer.models.RetailerModel;
import com.lokalchat.retailer.network.HttpConnection;
import com.lokalchat.retailer.utils.Constants;
import com.lokalchat.retailer.utils.Utils;

import org.apache.commons.collections4.MultiMap;
import org.apache.commons.collections4.map.MultiValueMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Sudipta on 9/26/2015.
 */
public class ChatTask {

    private Activity context;
    private HttpConnection httpConnect;
    private ChatListener chatListener;
    private String pageCounter = "0";
    private boolean isFirstTime;
    private boolean isLoadMore;
    private int limit;
    private boolean isFromCounter; // isFromCounter = isFromAlertDialog

    public ChatTask(Activity context, ChatListener chatListener, String pageCounter, boolean isLoadMore, boolean isFirstTime, boolean isFromCounter, int limit) {
        this.context = context;
        this.chatListener = chatListener;
        this.pageCounter = pageCounter;
        this.isFirstTime = isFirstTime;
        this.isLoadMore = isLoadMore;
        this.limit = limit;
        this.isFromCounter = isFromCounter;
    }

    public void fetchChat() {
        if (!isFirstTime && !isLoadMore && !isFromCounter && DBAdapter.getRecordsCount(context) > 0) {
            onChatFragmentVisible();
        } else if (isLoadMore) {
            int onlineRecords = Utils.getShopChatApplication(context).getTotalOnlineRecord();
            int dbRecords = DBAdapter.getRecordsCount(context);

            if (dbRecords < onlineRecords) {
                fetchChatOnline();
            } else {
                fetchChatOffline();
            }
        } else {
            fetchChatOnline();
        }
    }

    private void fetchChatOnline() {
        if (!Utils.isConnectionPossible(this.context)) {
            ErrorModel errorModel = new ErrorModel();
            errorModel.setErrorType(ErrorModel.Error.ERROR_TYPE_NO_NETWORK);
            // TODO used actual text
            errorModel.setErrorMessage(context.getResources().getString(R.string.error_no_network));
            chatListener.onChatFetchFailure(errorModel);

        } else {
            initNetworkTask();
        }
    }

    private void onChatFragmentVisible() {
        final Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        fetchChatOffline();
                    }
                });

                timer.cancel();
            }
        }, 500, 500);
    }

    private void fetchChatOffline() {
        chatListener.onChatFetchSuccess(DBAdapter.getChatRecords(context, limit));
    }

    private void initNetworkTask() {

        Handler networkHandler = new Handler() {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case HttpConnection.DID_START:
                        chatListener.onChatFetchStart();
                        break;
                    case HttpConnection.DID_SUCCEED:
                        String jsonResponse = (String) message.obj;
                        getChatList(jsonResponse);
                        fetchChatOffline();
                        break;
                    case HttpConnection.DID_UNSUCCESS:
                        ErrorModel errorModel = new ErrorModel();
                        errorModel.setErrorType(ErrorModel.Error.ERROR_TYPE_SERVER);
                        // TODO used actual text
                        errorModel.setErrorMessage(context.getResources().getString(R.string.error_server));
                        chatListener.onChatFetchFailure(errorModel);
                        break;
                    case HttpConnection.DID_ERROR:
                        ErrorModel errorModel2 = new ErrorModel();
                        errorModel2.setErrorType(ErrorModel.Error.ERROR_TYPE_SERVER);
                        // TODO used actual text
                        errorModel2.setErrorMessage(context.getResources().getString(R.string.error_server));
                        chatListener.onChatFetchFailure(errorModel2);
                        break;
                    default:
                        break;
                }
            }
        };

        httpConnect = new HttpConnection(networkHandler);

        final String chatUrl = Constants.BASE_URL + Constants.CHAT_URL;
        LoginModel loginModel = ((ShopChatApplication) context.getApplicationContext()).getLoginModel();
        httpConnect.post(chatUrl, null, getRequestBody(), HttpConnection.REQUEST_COMMON, loginModel);
    }

    private String getRequestBody() {
        String requestBody = null;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("pageNumber", pageCounter);
            jsonObject.put("size", "5");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        requestBody = jsonObject.toString();
        return requestBody;
    }

    public List<ProductModel> getChatList(String response) {
        List<ProductModel> productModelList = new ArrayList<ProductModel>();

        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray contentArray = jsonObject.getJSONArray("content");
            ProductModel productModel = null;
            for (int ii = 0; ii < contentArray.length(); ii++) {
                //ChatModel chatModel = new ChatModel();
                JSONObject contentObject = contentArray.getJSONObject(ii);
                JSONObject questionObject = contentObject.getJSONObject("question");
                JSONObject productObject = questionObject.getJSONObject("product");

                productModel = new ProductModel();
                productModel.setConsumerChatId(contentObject.getString("id"));
                productModel.setLastModifiedDate(contentObject.getString("lastModifiedDate"));
                productModel.setProductId(productObject.getString("id"));
                productModel.setProductName(productObject.getString("productName"));
                productModel.setConsumerChatContent(questionObject.getString("questionText"));

                List<RetailerModel> retailerModelList = new ArrayList<RetailerModel>();
                JSONObject retailerObject = contentObject.getJSONObject("retailer");
                RetailerModel retailerModel = new RetailerModel();
                retailerModel.setRetailerId(retailerObject.getString("id"));
                retailerModel.setRetailerName(retailerObject.getString("shopName"));
                retailerModel.setRetailerChatContent(contentObject.getString("answerText"));
                retailerModelList.add(retailerModel);


                productModel.setRetailerModels(retailerModelList);
                productModel.setConsumerChatContent(questionObject.getString("questionText"));
                productModelList.add(productModel);

                int totalRecords = DBAdapter.getRecordsCount(context);
                Log.d(Constants.TAG, "Total Number of Database records:" + totalRecords);
                boolean isChatExists = DBAdapter.isChatExists(context, productModel.getConsumerChatId());
                if (!isChatExists && productModel != null) {
                    DBAdapter.insertRecords(context, productModel);
                }
            }

            JSONObject pageObject = jsonObject.getJSONObject("page");
            String totalPage = pageObject.getString("totalPages");
            Utils.getShopChatApplication((Activity) context).setNumberOfChatPage(Integer.parseInt(totalPage));

            String currentPage = pageObject.getString("number");
            Utils.getShopChatApplication((Activity) context).setCurrentChatPageNumber(Integer.parseInt(currentPage));

            String totalOnlineRecords = pageObject.getString("totalElements");
            Utils.getShopChatApplication((Activity) context).setTotalOnlineRecord(Integer.parseInt(totalOnlineRecords));

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return productModelList;

    }

    private MultiMap<String, ProductModel> getChatMap(String response) {
        MultiMap<String, ProductModel> chatMap = new MultiValueMap<String, ProductModel>();

        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray contentArray = jsonObject.getJSONArray("content");
            for (int ii = 0; ii < contentArray.length(); ii++) {
                //ChatModel chatModel = new ChatModel();
                JSONObject contentObject = contentArray.getJSONObject(ii);
                JSONObject productObject = contentObject.getJSONObject("product");

                ProductModel productModel = new ProductModel();
                productModel.setProductId(productObject.getString("id"));
                productModel.setProductName(productObject.getString("productName"));
                productModel.setChatTimeStamp(productObject.getString("lastModifiedDate"));

                List<RetailerModel> retailerModelList = new ArrayList<RetailerModel>();
                JSONArray answerArray = contentObject.getJSONArray("answers");
                for (int jj = 0; jj < answerArray.length(); jj++) {
                    JSONObject answerObject = answerArray.getJSONObject(jj);
                    JSONObject retailerObject = answerObject.getJSONObject("retailer");
                    RetailerModel retailerModel = new RetailerModel();
                    retailerModel.setRetailerId(retailerObject.getString("id"));
                    retailerModel.setRetailerName(retailerObject.getString("shopName"));
                    retailerModel.setRetailerChatContent(answerObject.getString("answerText"));
                    retailerModel.setChatTimeStamp(answerObject.getString("lastModifiedDate"));
                    retailerModelList.add(retailerModel);
                }

                productModel.setRetailerModels(retailerModelList);
                productModel.setConsumerChatContent(contentObject.getString("questionText"));
                chatMap.put(productModel.getProductId(), productModel);

                JSONObject pageObject = jsonObject.getJSONObject("page");
                String totalPage = pageObject.getString("totalPages");
                Utils.getShopChatApplication((Activity) context).setNumberOfChatPage(Integer.parseInt(totalPage));

                String currentPage = pageObject.getString("number");
                Utils.getShopChatApplication((Activity) context).setCurrentChatPageNumber(Integer.parseInt(currentPage));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return chatMap;
    }

}
