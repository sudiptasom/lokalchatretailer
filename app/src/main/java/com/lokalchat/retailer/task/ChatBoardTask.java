package com.lokalchat.retailer.task;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;


import com.lokalchat.retailer.R;
import com.lokalchat.retailer.ShopChatApplication;
import com.lokalchat.retailer.listener.ChatBoardListener;
import com.lokalchat.retailer.models.ChatBoardModel;
import com.lokalchat.retailer.models.ErrorModel;
import com.lokalchat.retailer.models.LoginModel;
import com.lokalchat.retailer.models.ProductModel;
import com.lokalchat.retailer.models.RetailerModel;
import com.lokalchat.retailer.network.HttpConnection;
import com.lokalchat.retailer.utils.Constants;
import com.lokalchat.retailer.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Sudipta on 9/27/2015.
 */
public class ChatBoardTask {

    private Context context;
    private HttpConnection httpConnect;
    private ChatBoardListener chatBoardListener;
    private String message;
    private RetailerModel retailerModel;
    private ProductModel productModel;

    public ChatBoardTask(Context context, ChatBoardListener chatBoardListener) {
        this.context = context;
        this.chatBoardListener = chatBoardListener;
        this.message = message;
        this.retailerModel = retailerModel;
        this.productModel = productModel;

    }

    public void sendChat() {
        if (!Utils.isConnectionPossible(this.context)) {
            ErrorModel errorModel = new ErrorModel();
            errorModel.setErrorType(ErrorModel.Error.ERROR_TYPE_NO_NETWORK);
            // TODO used actual text
            errorModel.setErrorMessage(context.getResources().getString(R.string.error_no_network));
            chatBoardListener.onSendChatFailure(errorModel);

        } else {
            initNetworkTask();
        }
    }

    private void initNetworkTask() {

        Handler networkHandler = new Handler() {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case HttpConnection.DID_START:
                        chatBoardListener.onSendChatStart();
                        break;
                    case HttpConnection.DID_SUCCEED:
                        String jsonResponse = (String) message.obj;
                        chatBoardListener.onSendChatSuccess();
                        break;
                    case HttpConnection.DID_UNSUCCESS:
                        ErrorModel errorModel = new ErrorModel();
                        errorModel.setErrorType(ErrorModel.Error.ERROR_TYPE_SERVER);
                        // TODO used actual text
                        errorModel.setErrorMessage(context.getResources().getString(R.string.error_server));
                        chatBoardListener.onSendChatFailure(errorModel);
                        break;
                    case HttpConnection.DID_ERROR:
                        ErrorModel errorModel2 = new ErrorModel();
                        errorModel2.setErrorType(ErrorModel.Error.ERROR_TYPE_SERVER);
                        // TODO used actual text
                        errorModel2.setErrorMessage(context.getResources().getString(R.string.error_server));
                        chatBoardListener.onSendChatFailure(errorModel2);
                        break;
                    default:
                        break;
                }
            }
        };

        httpConnect = new HttpConnection(networkHandler);

        final String chatSubmitUrl = Constants.BASE_URL + Constants.CHAT_SUBMIT_URL;
        LoginModel loginModel = ((ShopChatApplication) context.getApplicationContext()).getLoginModel();
        httpConnect.post(chatSubmitUrl, null, getRequestBody(), HttpConnection.REQUEST_COMMON, loginModel);
    }

    private String getRequestBody() {
        String requestBody = null;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("answerid", getChatId());
            jsonObject.put("answer", getMessage());


        } catch (JSONException e) {
            e.printStackTrace();
        }
        requestBody = jsonObject.toString();
        return requestBody;
    }

    private String getRetailerId() {
        List<ChatBoardModel> chatBoardList = Utils.getShopChatApplication((Activity) this.context).getChatBoardModels();
        StringBuilder retailerBuilder = new StringBuilder();
        for (ChatBoardModel chatBoardModel : chatBoardList) {
            if (!chatBoardModel.isCustomer()) {
                RetailerModel retailerModel = chatBoardModel.getRetailerModel();
                if (retailerModel != null) {
                    retailerBuilder.append(retailerModel.getRetailerId());
                    retailerBuilder.append(",");
                }
            }
        }

        return retailerBuilder.toString();
    }

    private String getChatId() {
        List<ChatBoardModel> chatBoardList = Utils.getShopChatApplication((Activity) this.context).getChatBoardModels();
        ChatBoardModel chatBoardModel = chatBoardList.get(1);
        return chatBoardModel.getProductModel().getConsumerChatId();

    }

    private String getMessage() {
        List<ChatBoardModel> chatBoardList = Utils.getShopChatApplication((Activity) this.context).getChatBoardModels();
        ChatBoardModel chatBoardModel = chatBoardList.get(1);
        return chatBoardModel.getChatContent();
    }

}
