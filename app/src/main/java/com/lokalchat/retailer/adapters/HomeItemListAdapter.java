package com.lokalchat.retailer.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.lokalchat.retailer.R;
import com.lokalchat.retailer.models.HomeItemModel;

import java.util.List;

/**
 * Created by Sudipta on 8/18/2015.
 */
public class HomeItemListAdapter extends BaseAdapter {
    private Context context;
    private int resourceId;
    private List<HomeItemModel> itemList;

    /**
     * @param context
     * @param resourceId
     * @param itemList
     */
    public HomeItemListAdapter(Context context, int resourceId, List<HomeItemModel> itemList) {
        this.context = context;
        this.resourceId = resourceId;
        this.itemList = itemList;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        View view = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(resourceId, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvItemTitle = (TextView) view.findViewById(R.id.tv_item_title);
            viewHolder.ivItemImage = (ImageView) view.findViewById(R.id.iv_item_image);

            view.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        HomeItemModel homeItemModel = itemList.get(position);
        viewHolder.tvItemTitle.setText(homeItemModel.getItemName());
        viewHolder.ivItemImage.setImageResource(homeItemModel.getItemPicture());


        return view;
    }

    public class ViewHolder {
        TextView tvItemTitle;
        ImageView ivItemImage;

    }
}
