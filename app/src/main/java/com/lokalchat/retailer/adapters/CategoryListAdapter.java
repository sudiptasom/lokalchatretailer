package com.lokalchat.retailer.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lokalchat.retailer.R;
import com.lokalchat.retailer.models.CategoryModel;
import com.lokalchat.retailer.utils.Constants;
import com.lokalchat.retailer.utils.imageloader.ImageLoaderPlayed;

import java.util.List;

/**
 * Created by Sudipta on 8/18/2015.
 */
public class CategoryListAdapter extends BaseAdapter {
    private Context context;
    private int resourceId;
    private List<CategoryModel> itemList;

    /**
     * @param context
     * @param resourceId
     * @param itemList
     */
    public CategoryListAdapter(Context context, int resourceId, List<CategoryModel> itemList) {
        this.context = context;
        this.resourceId = resourceId;
        this.itemList = itemList;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        View view = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(resourceId, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvItemTitle = (TextView) view.findViewById(R.id.tv_item_title);
            viewHolder.ivItemImage = (ImageView) view.findViewById(R.id.iv_item_image);

            view.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        CategoryModel homeItemModel = itemList.get(position);
        viewHolder.tvItemTitle.setText(homeItemModel.getItemName());
        String imageUrl = Constants.BASE_URL + "/image/" + homeItemModel.getItemPictureUrl();
        try {
            ImageLoaderPlayed imageLoaderLast = new ImageLoaderPlayed(
                    context);
            imageLoaderLast.DisplayImage(imageUrl, viewHolder.ivItemImage);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    public class ViewHolder {
        TextView tvItemTitle;
        ImageView ivItemImage;

    }
}
