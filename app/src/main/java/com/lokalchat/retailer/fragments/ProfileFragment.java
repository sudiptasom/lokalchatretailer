package com.lokalchat.retailer.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lokalchat.retailer.R;
import com.lokalchat.retailer.activities.ChangePasswordActivity;
import com.lokalchat.retailer.activities.LandingActivity;
import com.lokalchat.retailer.utils.Constants;
import com.lokalchat.retailer.utils.Utils;
import com.lokalchat.retailer.views.ActionBarHome;


/**
 * Created by Sudipta on 8/9/2015.
 */
public class ProfileFragment extends Fragment {

    private EditText tvName;
    private EditText tvEmail;
    private EditText tvPhone;
    private EditText tvStreet;
    private EditText tvCity;
    private EditText tvState;
    private EditText tvPin;
    private ActionBarHome actionBarHome;
    private boolean isKeyBoardVisible;
    private TextView tvEditProfile;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((LandingActivity) getActivity()).setSaveClickListener(saveClickListener);

    }

    @Override
    public void onResume() {
        super.onResume();
        actionBarHome = ((LandingActivity) getActivity()).getActivityActionBar();
        populateData();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_profile, null);

        tvName = (EditText) rootView.findViewById(R.id.tv_name);
        tvEmail = (EditText) rootView.findViewById(R.id.tv_email);
        tvPhone = (EditText) rootView.findViewById(R.id.tv_phone);
        tvEditProfile = (TextView) rootView.findViewById(R.id.tv_edit_profile);
        final Button btnLogout = (Button) rootView.findViewById(R.id.btn_logout);

      /*  tvStreet = (EditText) rootView.findViewById(R.id.tv_street_address);
        tvCity = (EditText) rootView.findViewById(R.id.tv_city);
        tvState = (EditText) rootView.findViewById(R.id.tv_state);
        tvPin = (EditText) rootView.findViewById(R.id.tv_pin_code);*/

        // To catch keyboard appear/disappear event
        final LinearLayout parentLayout = (LinearLayout) rootView.findViewById(R.id.parentLayout);
        parentLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect rectRoot = new Rect();
                parentLayout.getWindowVisibleDisplayFrame(rectRoot);
                int screenHeight = parentLayout.getRootView().getHeight();
                int keypadHeight = screenHeight - rectRoot.bottom;

                if (actionBarHome != null) {
                    if (actionBarHome.getVisibleFragment() != null && actionBarHome.getVisibleFragment() instanceof ProfileFragment) {
                        if (keypadHeight > screenHeight * 0.05) {
                            isKeyBoardVisible = true;
                        } else {
                            isKeyBoardVisible = false;
                        }
                    } else {
                        isKeyBoardVisible = false;
                    }

                }

            }
        });

        tvName.addTextChangedListener(textWatcher);
        tvEmail.addTextChangedListener(textWatcher);
        tvPhone.addTextChangedListener(textWatcher);

        tvEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = Constants.BASE_URL;
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Utils.showLogoutConfirmationDialog(getActivity());
            }
        });
     /*   tvStreet.addTextChangedListener(textWatcher);
        tvCity.addTextChangedListener(textWatcher);
        tvState.addTextChangedListener(textWatcher);
        tvPin.addTextChangedListener(textWatcher);*/

        final Button btnChangePassword = (Button) rootView.findViewById(R.id.btn_change_password);
        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(ChangePasswordActivity.getIntent(getActivity()));
            }
        });

        return rootView;
    }

    private void populateData() {
        tvName.setText(Utils.getPersistenceData(getActivity(), Constants.REGISTERED_NAME_PREFERENCE));
        tvEmail.setText(Utils.getPersistenceData(getActivity(), Constants.REGISTERED_EMAIL_PREFERENCE));
        tvPhone.setText(Utils.getPersistenceData(getActivity(), Constants.REGISTERED_PHONE_PREFERENCE));
       /* tvStreet.setText(Utils.getPersistenceData(getActivity(), Constants.PROFILE_STREET_PREFERENCE));
        tvCity.setText(Utils.getPersistenceData(getActivity(), Constants.PROFILE_CITY_PREFERENCE));
        tvState.setText(Utils.getPersistenceData(getActivity(), Constants.PROFILE_STATE_PREFERENCE));
        tvPin.setText(Utils.getPersistenceData(getActivity(), Constants.PROFILE_PIN_CODE_PREFERENCE));*/
    }


    private LandingActivity.SaveClickListener saveClickListener = new LandingActivity.SaveClickListener() {
        @Override
        public void onSaveClick() {

            if (!TextUtils.isEmpty(tvName.getText())) {
                Utils.setPersistenceData(getActivity(), Constants.REGISTERED_NAME_PREFERENCE, tvName.getText().toString());
            }
            if (!TextUtils.isEmpty(tvEmail.getText())) {
                Utils.setPersistenceData(getActivity(), Constants.REGISTERED_EMAIL_PREFERENCE, tvEmail.getText().toString());
            }

            if (!TextUtils.isEmpty(tvPhone.getText())) {
                Utils.setPersistenceData(getActivity(), Constants.REGISTERED_PHONE_PREFERENCE, tvPhone.getText().toString());
            }

           /* if (!TextUtils.isEmpty(tvStreet.getText())) {
                Utils.setPersistenceData(getActivity(), Constants.PROFILE_STREET_PREFERENCE, tvStreet.getText().toString());
            }

            if (!TextUtils.isEmpty(tvCity.getText())) {
                Utils.setPersistenceData(getActivity(), Constants.PROFILE_CITY_PREFERENCE, tvCity.getText().toString());
            }

            if (!TextUtils.isEmpty(tvState.getText())) {
                Utils.setPersistenceData(getActivity(), Constants.PROFILE_STATE_PREFERENCE, tvState.getText().toString());
            }

            if (!TextUtils.isEmpty(tvPin.getText())) {
                Utils.setPersistenceData(getActivity(), Constants.PROFILE_PIN_CODE_PREFERENCE, tvPin.getText().toString());
            }*/

            // TODO Localization
            Toast.makeText(getActivity(), "Your Profile has been saved successfully!", Toast.LENGTH_SHORT).show();
            Utils.hideKeyBoard(getActivity());
        }


    };


    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (actionBarHome != null && isKeyBoardVisible) {
                actionBarHome.showSaveMenu();
            }
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        isKeyBoardVisible = false;
    }
}
