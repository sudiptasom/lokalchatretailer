package com.lokalchat.retailer.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;


import com.lokalchat.retailer.R;
import com.lokalchat.retailer.adapters.ItemListAdapter;
import com.lokalchat.retailer.models.ItemModel;
import com.lokalchat.retailer.utils.SearchManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sudipta on 8/31/2015.
 */
public class ItemListFragment extends Fragment {

    private List list;
    private ItemListAdapter listAdapter;
    private ListView lvItemList;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_item_list, null);

        lvItemList = (ListView) rootView.findViewById(R.id.lv_item);
        final EditText searchEditText = (EditText) rootView.findViewById(R.id.edtText_search);
        searchEditText.addTextChangedListener(textWatcher);

        list = new ArrayList();
        list.add(new ItemModel("Apple"));
        list.add(new ItemModel("Boy"));
        list.add(new ItemModel("Cat"));
        list.add(new ItemModel("Dog"));
        list.add(new ItemModel("Elephant"));
        list.add(new ItemModel("Fan"));
        list.add(new ItemModel("Goat"));
        list.add(new ItemModel("Horse"));


        listAdapter = new ItemListAdapter(getActivity(), R.layout.list_row_item, list);
        lvItemList.setAdapter(listAdapter);

        return rootView;
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            SearchManager searchManager = new SearchManager(list, s.toString(), SearchManager.SearchableEnum.ITEM);
            searchManager.setSearchListener(searchListener);
            searchManager.initSearch();
        }
    };

    SearchManager.SearchListener searchListener = new SearchManager.SearchListener() {
        @Override
        public void onSearchSuccess(List<Object> searchedList) {
            listAdapter = new ItemListAdapter(getActivity(), R.layout.list_row_item, searchedList);
            lvItemList.setAdapter(listAdapter);
            lvItemList.invalidate();

        }

        @Override
        public void onSearchFail() {
            // TODO Localization
            //Toast.makeText(getActivity(), "No data found!", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onSearchStringEmpty() {
            listAdapter = new ItemListAdapter(getActivity(), R.layout.list_row_item, list);
            lvItemList.setAdapter(listAdapter);
            lvItemList.invalidate();
        }
    };
}
