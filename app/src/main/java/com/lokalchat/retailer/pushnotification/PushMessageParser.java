package com.lokalchat.retailer.pushnotification;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Sudipta on 4/3/2016.
 */
public class PushMessageParser {

    public static String getMessage(String response) {
        String message = "";
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONObject questionObject = jsonObject.getJSONObject("question");
            message = questionObject.getString("questionText");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return message;
    }
}
