package com.lokalchat.retailer.pushnotification;

import android.content.Intent;
import android.util.Log;

import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * @author nayanesh
 */
public class PushNotificationInstanceIDListenerService extends InstanceIDListenerService {

    public static final String TAG = "InstanceIDListenerServ";

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        Log.d(TAG, "In OnTokenRefresh");

        Intent intent = new Intent(this, PushNotificationIntentService.class);
        startService(intent);

    }

}
