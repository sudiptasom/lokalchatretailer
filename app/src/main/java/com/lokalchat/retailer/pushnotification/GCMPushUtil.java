package com.lokalchat.retailer.pushnotification;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

/**
 *  @author  Nayanesh Gupte
 */
public class GCMPushUtil {

    private static final String TAG = GCMPushUtil.class.getSimpleName();

    public static final String PROPERTY_REG_ID = "registration_id";

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;


    /**
     * @return if Google Service is available or not
     */
    public static boolean isGoogleServiceAvailable(Context context) {

        int resultCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (context instanceof Activity) {
                Activity activity = (Activity) context;
                if (GoogleApiAvailability.getInstance().isUserResolvableError(resultCode)) {
                    GoogleApiAvailability.getInstance().getErrorDialog(activity,resultCode,
                            PLAY_SERVICES_RESOLUTION_REQUEST).show();
                } else {
                    Log.d(TAG, "This device is not supported.");
                    activity.finish();
                }
            }
            return false;
        }
        return true;
    }

}
