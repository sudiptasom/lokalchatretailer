package com.lokalchat.retailer.pushnotification;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.lokalchat.retailer.R;
import com.lokalchat.retailer.utils.Constants;
import com.lokalchat.retailer.utils.Utils;


/**
 * @author nayanesh
 */
public class PushNotificationHelper {

    private static final String TAG = PushNotificationHelper.class.getSimpleName();

    public static final String PROJECT_ID = "project_id";
    public static final String RECEIVER = "receiver";

    private Context mContext;


    private OnPushRegisterListener onPushRegisterListener;

    public PushNotificationHelper(Context mContext) {
        this.mContext = mContext;

    }

    public void setOnPushRegisterListener(OnPushRegisterListener onPushRegisterListener) {
        this.onPushRegisterListener = onPushRegisterListener;
    }

    /**
     * Registering with GCM for to receive Push Notifications
     */
    public static void pushNotificationProcess(Context mContext,PushResultReceiver pushResultReceiver) {
        String registrationId = Utils.getPersistenceData(mContext, Constants.PUSH_REGISTRATION_ID_PREFERENCE);

        //If App version changed OR value from preference is empty, register again for push Notification.
        if (registrationId.isEmpty()) {

            //Push Notification Registration

            if (GCMPushUtil.isGoogleServiceAvailable(mContext)) {
                initiateRegistration(mContext,mContext.getString(R.string.project_id_dev), pushResultReceiver);
            }
        }
    }


    public String getGCMRegistrationId() {

        return Utils.getPersistenceData(mContext, Constants.PUSH_REGISTRATION_ID_PREFERENCE);

    }


    public boolean isGCMRegistered() {

        if (TextUtils.isEmpty(Utils.getPersistenceData(mContext, Constants.PUSH_REGISTRATION_ID_PREFERENCE))) {
            return false;
        }
        return true;
    }


    /**
     * Initiates registration with the project id. App needs have receiver to receive broadacst which consists of regId
     *
     * @param projectNumber Project Number (Sender Id received from google console)
     * @return
     */
    public static void initiateRegistration(Context mContext,String projectNumber, PushResultReceiver receiver) {

        Intent intent = new Intent(mContext, PushNotificationIntentService.class);
        intent.putExtra(PROJECT_ID, projectNumber);
        intent.putExtra(RECEIVER, receiver);
        mContext.startService(intent);

    }


}



