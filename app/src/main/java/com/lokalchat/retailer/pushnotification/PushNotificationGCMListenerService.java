package com.lokalchat.retailer.pushnotification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;
import com.lokalchat.retailer.R;
import com.lokalchat.retailer.activities.LoginActivity;
import com.lokalchat.retailer.utils.Constants;

/**
 * @author Nayanesh Gupte
 */
public class PushNotificationGCMListenerService extends GcmListenerService {

    private final static String TAG = PushNotificationGCMListenerService.class.getSimpleName();


    @Override
    public void onMessageReceived(String from, Bundle data) {
        super.onMessageReceived(from, data);

        Log.d(TAG, "onMessageReceived from: " + from + " data: " + data);

        String pushData = data.getString("message");
        String userData = data.getString("userdata");
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setContentTitle(pushData);
        mBuilder.setContentText(PushMessageParser.getMessage(userData));
        //mBuilder.setContentText("Push Message");
        mBuilder.setTicker("New Message Alert!");
        mBuilder.setSmallIcon(R.drawable.ic_launcher);
        mBuilder.setAutoCancel(true);

      /*  NotificationCompat.InboxStyle inboxStyle =
                new NotificationCompat.InboxStyle();
        inboxStyle.addLine(PushMessageParser.getMessage(userData));
        mBuilder.setStyle(inboxStyle);*/

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Intent notificationIntent = new Intent(this, LoginActivity.class);
        notificationIntent.putExtra(Constants.IS_FROM_PUSH, true);

        PendingIntent intent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);
        mBuilder.setContentIntent(intent);
        mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        mNotificationManager.notify(0, mBuilder.build());
    }

    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }

    @Override
    public void onMessageSent(String msgId) {
        super.onMessageSent(msgId);
    }


    // Send an Intent with an action named "my-event".
    private void sendMessage(int unReadNotificationCount) {

    }


}

