package com.lokalchat.retailer.pushnotification;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

/**
 * @author nayanesh
 */
public class PushResultReceiver extends ResultReceiver {

    private PushIntentReceiver mReceiver;

    /**
     * @param handler
     */
    public PushResultReceiver(Handler handler) {
        super(handler);

    }

    /**
     * @author nayanesh
     */
    public interface PushIntentReceiver {
        void onReceiveResult(int resultCode, Bundle resultData);

    }

    public void setReceiver(PushIntentReceiver receiver) {
        mReceiver = receiver;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {

        if (mReceiver != null) {
            mReceiver.onReceiveResult(resultCode, resultData);
        }
    }

}