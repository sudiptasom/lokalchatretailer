package com.lokalchat.retailer.pushnotification;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;


/**
 * @author Nayanesh Gupte
 */
public class PushNotificationIntentService extends IntentService {

    public static final String TAG = PushNotificationIntentService.class.getSimpleName();
    public static final String REG_ID = "reg_id";

    private ResultReceiver receiver;

    public PushNotificationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {

            String projectId = intent.getStringExtra(PushNotificationHelper.PROJECT_ID);
            receiver = intent.getParcelableExtra(PushNotificationHelper.RECEIVER);

            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(projectId, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

            Log.e(TAG,"push GCM Registration Token: " + token);

            //If non-blank, Saving the regId into Preferences and sending broadcast to the app
            if (!token.isEmpty()) {

                Bundle b = new Bundle();
                b.putString(REG_ID, token);
                receiver.send(0, b);

            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
