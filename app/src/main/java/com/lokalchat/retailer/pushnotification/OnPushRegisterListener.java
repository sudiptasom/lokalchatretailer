package com.lokalchat.retailer.pushnotification;

/**
 * @author  Nayanesh Gupte
 */
public interface OnPushRegisterListener {

    void onPushRegistered();

    void onPushRegisterFaild();
}
